<?php
require_once '../vendor/autoload.php';
require_once '../src/includes/enableAuth.php';
$smh = new SmHelper;
$leaderboardEnabled = (isset($leaderboardEnabled))?$leaderboardEnabled:false;
$loggedIn = false;
if(isset($headerFunction)){
	if(is_callable($headerFunction)){
		$headerFunction();
	}
}
if ($auth->isLogged()) {
    $loggedIn = true;
		$hash = $auth->getSessionHash();
		$uid = $auth->getSessionUID($hash);
		$smh->setSessionData($uid);
} else {
	if(isset($_SESSION['gd'])){
		session_unset();
	}
}
$crumbs = explode("/",$_SERVER["REQUEST_URI"]);
$crumbs = array_filter($crumbs);
$result = array(); $path = '';
$count = count($crumbs);
foreach($crumbs as $k=>$crumb){
	 $path .= '/' . $crumb;
	 $name = str_replace(array(".php","_"),array(""," "), $crumb);
	 if($k == $count){
		 $currentPage = $name;
	 }
};
?>
<!DOCTYPE html>
<html lang="en">

<head>
<?php include '../src/includes/link_setup.php'; ?>
	<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <!-- Tell the browser to be responsive to screen width -->
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, minimal-ui">
			<meta name="description" content="Mine for your favorite stream!">
			<meta name="author" content="Stream Miner">
			<meta name="apple-mobile-web-app-capable" content="yes" />
			<meta name="apple-mobile-web-app-status-bar-style" content="black" />
			<meta name="HandheldFriendly" content="true" />
			<meta name="mobile-web-app-capable" content="yes" />
			<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
			<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
			<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
			<link rel="manifest" href="/manifest.json">
			<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
			<meta name="theme-color" content="#ffffff">



			<title><?php echo isset($headTitle)?$headTitle:"Stream Miner";?></title>

      <!-- Bootstrap Core CSS -->
      <link href="<?php echo $homeUrl;  ?>assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
			<!-- This is a Animation CSS -->
      <link href="<?php echo $homeUrl;  ?>assets/css/animate.css" rel="stylesheet">
      <!-- This is Sidebar menu CSS -->
      <link href="<?php echo $homeUrl;  ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
      <!-- This is a Custom CSS -->
      <link href="<?php echo $homeUrl;  ?>assets/css/style.css" rel="stylesheet">
      <!-- color CSS you can use different color css from css/colors folder -->
      <!-- We have chosen the skin-blue (blue.css) for this starter
         page. However, you can choose any other skin from folder css / colors .
         -->
      <link href="<?php echo $homeUrl;  ?>assets/css/colors/gray.css" id="theme"  rel="stylesheet">
			<?php require_once '../src/includes/plugin-css-switch.php'; ?>
			<link href="https://fonts.googleapis.com/css?family=Orbitron:700" rel="stylesheet"> 
			<link href="<?php echo $homeUrl;  ?>assets/css/page.css" rel="stylesheet" type="text/css">
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
			<script>
				(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
				})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

				ga('create', 'UA-63915780-6', 'auto');
				ga('send', 'pageview');

			</script>
		<?php 
		if(isset($_SESSION['gd'])){
			$gd = $_SESSION['gd'];
			$points = $gd['points'];
			$tool = $gd['tool'];
			$expire = $gd['expire'];
			echo "<script type=\"text/javascript\">var points = $points; var toolLevel = $tool;</script>";
		};
		if(isset($noMenus)){
			
		};
		?>
