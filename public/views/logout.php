<?php 
$noMenus = true;
$themePlugins = array("sweetAlerts"); 
include __DIR__ . '/headerOpen.php';
?>
<?php
include __DIR__ . '/headerClose_Nav.php'; 
?>

<section id="wrapper" class="login-register orbitron">
	<div id="scrollDiv">
		<div class="login-box">
			<a href="<?php echo $homeUrl; ?>"><img src="<?php echo $homeUrl;  ?>assets/img/TextLogo500.png" class="img-responsive" alt="Stream Miner" /></a>
			<?php 
			if(isset($_COOKIE['authID'])){
				session_unset();
				$auth->logout($_COOKIE['authID']); ?>
			<div class="panel panel-success m-t-20">
				<div class="panel-heading"> Logged Out!</div>
				<div class="panel-wrapper collapse in" aria-expanded="true">
					<div class="panel-body">
						<?php echo (isset($timeoutNotice))?"<div class=\"alert alert-danger\"> You were logged out due to inactivity. </div>":""; ?>
						<p>You have logged out. Come back soon! The boulder will miss you :(</p>
						<a href="<?php echo $homeUrl; ?>home"><button class="btn btn-default m-t-10 pull-left">Back Home</button></a>
						<a href="<?php echo $homeUrl; ?>login"><button class="btn btn-success m-t-10 pull-right">Login</button></a>
					</div>
				</div>
			</div>
			<?php 
			} else {
				session_unset();
			?>
			<div class="panel panel-warning m-t-20">
				<div class="panel-heading"> Logged Out!</div>
				<div class="panel-wrapper collapse in" aria-expanded="true">
					<div class="panel-body">
						<?php echo (isset($timeoutNotice))?"<div class=\"alert alert-danger\"> You were logged out due to inactivity. </div>":""; ?>
						<p>You have logged out. Come back soon! The boulder will miss you :(</p>
						<a href="<?php echo $homeUrl; ?>home"><button class="btn btn-default m-t-10 pull-left">Back Home</button></a>
						<a href="<?php echo $homeUrl; ?>login"><button class="btn btn-success m-t-10 pull-right">Login</button></a>
					</div>
				</div>
			</div>
			<?php }; ?>
		</div>
	</div>
</section>
<?php 
$noGame = true;
include __DIR__ . '/javascripts.php';
?>
<script type="text/javascript">
if($(".login-box").height() > $(window).height()){
	$('#scrollDiv').slimScroll({
			height: $(window).height() + "px"
	});
}
</script>
<?php
include __DIR__ . '/pageClose.php'; 
?>
