<?php 
$pageTitle = "Stream Miner";
$leaderboardEnabled = true;
$themePlugins = array("sweetAlerts", "toast");
include __DIR__ . '/headerOpen.php'; 
if(!isset($daily_leader)){
	$daily_leader = $smh->getDailyLeaderData();
};

if($loggedIn){
	if(isset($_COOKIE["A_102716"])){
		if($_COOKIE["A_102716"] == 1){
			$hide1027 = true;
		} else {
			$hide1027 = false;
		};
	} else {
		$uid = $gd['uid'];
		$db = new DB();
		$cutoff = strtotime("October 27 2016");
		$db->bind("id",$uid);
		$joined = $db->single("SELECT joined FROM game_data WHERE id = :id");
		if ($joined <= $cutoff && $joined != null){
			$hide1027 = false;
		} else {
			$hide1027 = true;
		}
	}
} else {
	$hide1027 = true;
}

?>

<?php
include __DIR__ . '/headerClose_Nav.php';

?>

<div class="container-fluid">
  <div class="row a-p">
		<?php
		if(!$hide1027){
			echo "<div class=\"alert alert-info alert-dismissable m-b-0\">
              <button type=\"button\" class=\"close\" id=\"hide1027alert\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>
              We wanted to let you know that the shitty point saving system has been updated, and you've been gifted 5000 points to make up for some of the points you may have lost!</div>";
		};
		?>
    <div class="col-lg-9 no-padding">
			<div class="top-space">
      <?php 
				include '../src/includes/daily_stream.php';
				include '../src/includes/daily_chat.php';
			?>
			</div>
    </div>
    <div class="col-lg-3 no-padding">
      <div id="gameDiv"></div>
			<?php if(!$loggedIn){ ?><div class="alert alert-danger">Must be logged in to save progress!</div><?php };
			if (isset($_SESSION['gd']['representing'])){
				$stream = $_SESSION['gd']['representing'];
				echo "<div class=\"alert alert-info\">Representing: <a href=\"{$homeUrl}/u/$stream\" style=\"color:white\">$stream</a></div>";
			};
			include '../src/includes/leaderboard_stream_top_100.php'; ?>
    </div>
  </div>

</div>
<!-- /.container-fluid -->

<?php 
include __DIR__ . '/javascripts.php'; 
?>
<script type="text/javascript">
	$(document).ready(function(){
		gameStart();
	})
</script>
<?php 
include __DIR__ . '/pageClose.php'; 
?>
