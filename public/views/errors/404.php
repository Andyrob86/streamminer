<?php 
$noMenus = true;
include __DIR__ . '/../headerOpen.php'; 
?>

<?php
include __DIR__ . '/../headerClose_Nav.php'; 
?>
<section id="wrapper" class="error-page">
  <div class="error-box">
    <div class="error-body text-center">
      <h1>404</h1>
      <h3 class="text-uppercase">Page Not Found !</h3>
      <p class="text-muted m-t-30 m-b-30">CAN'T SEEM TO FIND THE PAGE YOU ARE LOOKING FOR</p>
      <a href="<?php echo $homeUrl; ?>" class="btn btn-info btn-rounded waves-effect waves-light m-b-40">Back to home</a> </div>
    <footer class="footer text-center">2016 © Stream Miner.</footer>
  </div>
</section>


<?php 
$noGame = true;
include __DIR__ . '/../javascripts.php'; 
include __DIR__ . '/../pageClose.php'; 
?>
