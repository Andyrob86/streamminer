</head>
<div class="preloader">
	 <div class="cssload-speeding-wheel"></div>
</div>
<?php if(!isset($noMenus)){ ?>
<body class="fix-sidebar">
	<!-- Preloader -->
	<div id="wrapper">
		 <!-- Top Navigation -->
		 <nav class="navbar navbar-default navbar-static-top m-b-0">
				<div class="navbar-header">
					 <!-- Toggle icon for mobile view -->
					 <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
					 <!-- Logo -->
					 <div class="top-left-part">
							<a class="logo" href="<?php echo $homeUrl; ?>">
								 <b><img src="<?php echo $homeUrl;  ?>assets/img/logo60.png" alt="Stream Miner" /></b>
								 <span class="hidden-xs"><img src="<?php echo $homeUrl;  ?>assets/img/TextLogo108.png" alt="" /></span>
							</a>
					 </div>
					 <ul class="nav navbar-top-links navbar-left hidden-xs">
						<li><a href="javascript:void(0)" class="open-close hidden-xs waves-effect waves-light"><i class="icon-arrow-left-circle ti-menu"></i></a></li>
					</ul>
					 <ul class="nav navbar-top-links navbar-right pull-right">
							<?php if($loggedIn) { ?>
							<!-- .user dropdown -->
							<li class="dropdown">
								 <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"><b class="hidden-xs"><?php echo $_SESSION['gd']['player']; ?></b> </a>
								 <ul class="dropdown-menu dropdown-user animated flipInY">
										<li><a href="<?php echo $homeUrl . "profile/" . $_SESSION['gd']['player']; ?>"><i class="ti-user"></i> My Profile</a></li>
										<li role="separator" class="divider"></li>
										<li><a href="<?php echo $homeUrl; ?>dashboard"><i class="ti-settings"></i> Account Setting</a></li>
										<li role="separator" class="divider"></li>
										<li><a href="#" class="userLogout"><i class="fa fa-power-off userLogout"></i> Logout</a></li>
								 </ul>
								 <!-- /.user dropdown-user -->
							</li>
							<!-- /.user dropdown -->
							<li class="right-side-toggle"> <a class="waves-effect waves-light" href="javascript:void(0)"><i class="ti-settings"></i></a></li>
							<?php } else { ?>
								<li><a href="<?php echo $homeUrl;?>login">Login / Register</a></li>
							
							<?php } ?>
					 </ul>
				</div>
				<!-- /.navbar-header -->
				<!-- /.navbar-top-links -->
				<!-- /.navbar-static-side -->
		 </nav>
		 <!-- End Top Navigation -->
		 <!-- Left navbar-sidebar -->
		 <div class="navbar-default sidebar" role="navigation">
				<div class="sidebar-nav navbar-collapse slimscrollsidebar">
					 <ul class="nav" id="side-menu">
							<?php if($loggedIn) { ?>
							<!-- User profile-->
							<li class="user-pro">
								 <a href="#" class="waves-effect"><span class="hide-menu"> <?php echo $_SESSION['gd']['player']; ?><span class="fa arrow"></span></span></a>
								 <ul class="nav nav-second-level">
										<li><a href="<?php echo $homeUrl . "profile/" . $_SESSION['gd']['player']; ?>"><i class="ti-user"></i> My Profile</a></li>
										<li><a href="<?php echo $homeUrl; ?>dashboard"><i class="ti-settings"></i> Account Setting</a></li>
										<li><a href="#" class="userLogout"><i class="fa fa-power-off userLogout"></i> Logout</a></li>
								 </ul>
							</li>
							<!-- User profile-->
							<?php } else { ?>
								<li><a href="<?php echo $homeUrl; ?>login" class="waves-effect"><i class="glyphicon glyphicon-log-in fa-fw"></i> <span class="hide-menu">Login / Register</span></a> </li>
							
							<?php } ?>
							<li class="nav-small-cap m-t-10">--- Main Menu</li>
							<li><a href="<?php echo $homeUrl; ?>home" class="waves-effect <?php echo ($pageTitle == "Stream Miner")?"active":""; ?>"><i class="fa fa-home fa-fw"></i><span class="hide-menu">  Home</span></a> </li>
							<li><a href="<?php echo $homeUrl; ?>game" class="waves-effect <?php echo ($pageTitle == "Stream Miner | Mining!")?"active":""; ?>"><i class="fa fa-gamepad fa-fw"></i><span class="hide-menu">  Game Only</span></a> </li>
							<li><a href="<?php echo $homeUrl; ?>about" class="waves-effect <?php echo ($pageTitle == "Stream Miner | About")?"active":""; ?>"><i class="fa fa-info-circle fa-fw"></i> <span class="hide-menu">  About</span></a> </li>
							<li class="nav-small-cap m-t-10">--- Game</li>
							<li><a href="<?php echo $homeUrl; ?>shop" class="waves-effect <?php echo ($pageTitle == "Stream Miner | Shop")?"active":""; ?>"><i class="zmdi zmdi-shopping-cart fa-fw "></i> <span class="hide-menu">  Miner Shop</span></a> </li>
							<li><a href="<?php echo $homeUrl; ?>represent" class="waves-effect <?php echo ($pageTitle == "Stream Miner | Represent!")?"active":""; ?>"><i class="ti ti-thumb-up fa-fw"></i> <span class="hide-menu">  Rep a Stream</span></a> </li>
							<li class="nav-small-cap m-t-10">--- Leaderboards</li>
							<li>
								 <a href="javascript:void(0)" class="waves-effect"><i data-icon="&#xe008;" class="fa fa-list-ol fa-fw"></i> <span class="hide-menu">Streams<span class="fa arrow"></span></span></a>
								 <ul class="nav nav-second-level">
									<li> <a href="<?php echo $homeUrl; ?>leaderboards/streams/daily">Daily</a> </li>
									<li> <a href="<?php echo $homeUrl; ?>leaderboards/streams/weekly">Weekly</a> </li>
									<li> <a href="<?php echo $homeUrl; ?>leaderboards/streams/monthly">Monthly</a> </li>
									<li> <a href="<?php echo $homeUrl; ?>leaderboards/streams/alltime">All-Time</a> </li>
								 </ul>
							</li>
							<li>
								 <a href="javascript:void(0)" class="waves-effect"><i data-icon="&#xe008;" class="fa fa-list-ol fa-fw"></i> <span class="hide-menu">Players<span class="fa arrow"></span></span></a>
								 <ul class="nav nav-second-level">
									<li> <a href="<?php echo $homeUrl; ?>leaderboards/players/daily">Daily</a> </li>
									<li> <a href="<?php echo $homeUrl; ?>leaderboards/players/weekly">Weekly</a> </li>
									<li> <a href="<?php echo $homeUrl; ?>leaderboards/players/monthly">Monthly</a> </li>
									<li> <a href="<?php echo $homeUrl; ?>leaderboards/players/alltime">All-Time</a> </li>
								 </ul>
							</li>
					 </ul>
				</div>
		 </div>
		 <!-- Left navbar-sidebar end -->
		 <!-- Page Content -->
		 <div id="page-wrapper">
<?php } else { ?>
<body>
<?php } ?>

