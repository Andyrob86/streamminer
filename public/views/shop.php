<?php
// $loggedIn is a boolean for user logged in state
// $homeUrl is local / remote specific URL var. Also var homeUrl in JS.
$headTitle = "Stream Miner | Miner Shop"; //default
$pageTitle = "Miner Shop"; //default
// $leaderboardEnabled = true; // Enable if leaderboards are present on page
// $noGame = true; // set to true if game will not be present on page
// $noMenus = true; // Disables sidebar and top nav
// $daily_leader will give current daily leader! surprise!
$themePlugins = array("sweetAlerts"); 
include __DIR__ . '/headerOpen.php'; 
//this area is between <head> and </head>
include __DIR__ . '/headerClose_Nav.php';

?>
<div class="container-fluid">
	<?php $smh->pageTitle($pageTitle); ?>
  <div class="row">
    <div class="col-lg-12 ">
		<?php 
		$td = new toolData();
		$tools = $td->all();
		$guest = array("tool" => 1, "points" => 0);
		$gd = (isset($_SESSION['gd']))?$_SESSION['gd']:$guest;
		if($loggedIn){
			echo "<div class=\"well m-b-30\"><h2>Available Points: {$gd['points']}</h2></div>";
		} else {
			echo "<div class=\"well m-b-30\"><h2>You must be signed in to purchase items</h2></div>";
		};
		foreach($tools as $id => $tool){
			$id = $id + 1; // shift for tool level
			if($gd['tool'] > $id){
				$shopClass = "default";
				$itemMessage = "<div class=\"well\"><h4>Purchased</h4><p>You already own this item.</p></div>";
				$purchaseButton = "";
				$btnDivClass = "";
			} elseif ($gd['tool'] == $id){
				$shopClass = "success";
				$itemMessage = "<div class=\"well\"><h4>Current Item</h4><p>This is your current item.</p></div>";
				$purchaseButton = "";
				$btnDivClass = "";
			} elseif ($gd['tool'] == $id - 1 ){
				
				if($tool['cost'] > $gd['points']){
					$shopClass = "danger";
					$itemMessage = "<div class=\"alert alert-danger\"> You do not have enough points saved up to purchase this item! </div>";
					$purchaseButton = "<button type=\"button\" id=\"item_{$id}_buy_btn\" onclick=\"purchaseItem({$id})\" class=\"btn btn-block btn-info top-space \"><h4>Purchase</h4></button>";
					$btnDivClass = "hidden";
				} elseif($tool['cost'] <= $gd['points']){
					$shopClass = "info";
					$itemMessage = "<div class=\"alert alert-success\"> Would you like to purchase this item? </div>";
					$purchaseButton = "<button type=\"button\" id=\"item_{$id}_buy_btn\" onclick=\"purchaseItem({$id})\" class=\"btn btn-block btn-info top-space \"><h4>Purchase</h4></button>";
					$btnDivClass = "";
				}
			} elseif ($gd['tool'] < $id -1){
					$shopClass = "danger";
					$itemMessage = "<div class=\"alert alert-danger\"> You must purchase the previous item before unlocking this one. </div>";
					$purchaseButton = "<button type=\"button\" id=\"item_{$id}_buy_btn\"  onclick=\"purchaseItem({$id})\" class=\"btn btn-block btn-info top-space \"><h4>Purchase</h4></button>";
					$btnDivClass = "hidden";
			}
			echo "<div id=\"item_{$id}_panel\" class=\"panel panel-{$shopClass}\"><div class=\"panel-heading tool-heading\" data-perform=\"panel-collapse\"> {$tool['name']} <div class=\"pull-right\"><a href=\"#\" data-perform=\"panel-collapse\"><i class=\"ti-plus\"></i></a></div></div><div class=\"panel-wrapper collapse\" aria-expanded=\"false\" style=\"height: 0px;\"><div class=\"panel-body\"><div class=\"row\"><div class=\"col-xs-12 col-md-2\"><div id=\"item_{$id}_image\"><img style=\"max-width:150px;\" class=\"center-block img-thumbnail\" src=\"assets/img/game/tools/{$id}.png\" alt=\"{$tool['name']}\"></div><hr class=\"enhancedHr\"><div id=\"item_{$id}_message_div\"><div id=\"item_{$id}_well_div\">{$itemMessage}</div><div id=\"item_{$id}_btn_div\" class=\"{$btnDivClass}\" >{$purchaseButton}</div></div></div><div class=\"col-xs-12 col-md-10\"><dl class=\"m-t-20\"><dt>Description</dt><dd id=\"item_{$id}_description\">{$tool['description']}</dd><hr class=\"enhancedHr\"><dt>Name</dt><dd id=\"item_{$id}_name\">{$tool['name']}</dd><br/><dt>Material</dt><dd id=\"item_{$id}_material\">{$tool['material']}</dd><br/><dt>Cost</dt><dd data-cost=\"{$tool['cost']}\" id=\"item_{$id}_cost\">{$tool['cost']}</dd><br/><dt>Points Per Click</dt><dd id=\"item_{$id}_ppc\">{$tool['ppc']}</dd></dl></div></div></div></div></div>";	
		}
		
		?>
		
    </div>
  </div>
</div>
<!-- /.container-fluid -->
<?php
$noGame = true;
include __DIR__ . '/javascripts.php'; 
include __DIR__ . '/pageClose.php'; 
?>
