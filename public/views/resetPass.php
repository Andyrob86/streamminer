<?php
// $loggedIn is a boolean for user logged in state
// $homeUrl is local / remote specific URL var. Also var homeUrl in JS.
 $headTitle = "Stream Miner | Reset Password"; //default - Used in <title> tag
// $pageTitle = "Stream Miner"; //default - Used on pages (breadcrumbs, and page titles)
// $leaderboardEnabled = true; // Enable if leaderboards are present on page
$noGame = true; // set to true if game will not be present on page
$noMenus = true; // Disables sidebar and top nav
// $daily_leader will give current daily leader! surprise!
// $smh variable gives access to Stream Miner function class

// $themePlguins is an array that will activate specific theme plugins' CSS and JS based on values in array.
/* 
Available theme plugins: 
		animateBg, bsDatePicker, bsDateRangePicker, bsRTLMaster, bsSelect, bsSocial, bsTable, bsTagsInput, bsTouchSpin, 
		bsTreeView, calendar, chart.js, chartist-js, clockpicker, colorpicker, counterup, cropper, css-chart, custom-select, dataTables, dataTables-Plugin, 
		dataTables-responsive, dropify, dropZone, etc, fancyBox, flot, flot.tooltip, footable, gallery, gmaps, holderjs, horizontal-timeline, html5-editor,
		ion-rangeslider, jquery, jquery.easy-pie-chart, jquery-asColorPicker, jquery-dataTables-editable, jquery-sparkline, jquery-wizard, jsgrind, magnific-popup,
		magnific-popup-master, mjolnic-bootstrap-colorpicker, mocha, moment, morrisjs, multiselect, nestable, owl.carousel, peity, raphael, sidebar-nav, skycons,
		styleSwitcher, summerNote, sweetAlert, switchery, tablesaw, timepicker, tiny-editable, tinymce, toast, typehead.js, vectormap, waypoints, x-editable

 */
$themePlugins = array("sweetAlerts"); 

include __DIR__ . '/headerOpen.php'; 
//this area is between <head> and </head>
include __DIR__ . '/headerClose_Nav.php';

?>    
<section id="wrapper" class="login-register orbitron">
	<div id="scrollDiv">
		<div class="login-box">
			<a href="<?php echo $homeUrl; ?>"><img src="<?php echo $homeUrl;  ?>assets/img/TextLogo500.png" class="img-responsive" alt="Stream Miner" /></a>
			<div class="white-box">
				<form class="form-horizontal form-material" method="POST" id="resetpassform" action="">
					<div class="form-group ">
						<div class="col-xs-12">
							<h3>Reset Password</h3>
							<p class="text-muted"><?php echo (isset($resetkey))?"Enter your new password below":"Enter your reset key, and new password below"; ?></p>
						</div>
					</div>
					<div class="form-group ">
						<div class="col-xs-12">
							<?php 
							if (isset($resetkey)){
								echo "<input id=\"resetKey\" type=\"hidden\" value=\"$resetkey\" >";
							} else { 
								echo "<input class=\"form-control\" id=\"resetKey\" type=\"text\" required=\"\" placeholder=\"Reset Key\" >";
							} 
							?>
						</div>
					</div>
					<div class="form-group ">
						<div class="col-xs-12">
							<input class="form-control" id="newpass" type="password" required="" placeholder="New Password">
						</div>
					</div>
					<div class="form-group ">
						<div class="col-xs-12">
							<input class="form-control" id="newpass2" type="password" required="" placeholder="Confirm Password">
						</div>
					</div>
					<div class="form-group text-center m-t-20">
						<div class="col-xs-12">
							<button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Reset</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
<!-- /.container-fluid -->
<?php
include __DIR__ . '/javascripts.php';
?>
<script type="text/javascript">
if($(".login-box").height() > $(window).height()){
	$('#scrollDiv').slimScroll({
			height: $(window).height() + "px"
	});
}
</script>
<?php
include __DIR__ . '/pageClose.php'; 
?>