<?php
// $loggedIn is a boolean for user logged in state
// $homeUrl is local / remote specific URL var. Also var homeUrl in JS.
$headTitle = "Stream Miner | Represent"; //default - Used in <title> tag
$pageTitle = "Represent a Streamer"; //default - Used on pages (breadcrumbs, and page titles)
// $leaderboardEnabled = true; // Enable if leaderboards are present on page
// $noGame = true; // set to true if game will not be present on page
// $noMenus = true; // Disables sidebar and top nav
// $daily_leader will give current daily leader! surprise!
// $smh variable gives access to Stream Miner function class

// $themePlguins is an array that will activate specific theme plugins' CSS and JS based on values in array.
/* 
Available theme plugins: 
		animateBg, bsDatePicker, bsDateRangePicker, bsRTLMaster, bsSelect, bsSocial, bsTable, bsTagsInput, bsTouchSpin, 
		bsTreeView, calendar, chart.js, chartist-js, clockpicker, colorpicker, counterup, cropper, css-chart, custom-select, dataTables, dataTables-Plugin, 
		dataTables-responsive, dropify, dropZone, etc, fancyBox, flot, flot.tooltip, footable, gallery, gmaps, holderjs, horizontal-timeline, html5-editor,
		ion-rangeslider, jquery, jquery.easy-pie-chart, jquery-asColorPicker, jquery-dataTables-editable, jquery-sparkline, jquery-wizard, jsgrind, magnific-popup,
		magnific-popup-master, mjolnic-bootstrap-colorpicker, mocha, moment, morrisjs, multiselect, nestable, owl.carousel, peity, raphael, sidebar-nav, skycons,
		styleSwitcher, summerNote, sweetAlert, switchery, tablesaw, timepicker, tiny-editable, tinymce, toast, typehead.js, vectormap, waypoints, x-editable

 */
$themePlugins = array("sweetAlerts"); 

include __DIR__ . '/headerOpen.php'; 
//this area is between <head> and </head>
include __DIR__ . '/headerClose_Nav.php';

?>    
<div class="container-fluid">
<?php $smh->pageTitle($pageTitle); 
if($loggedIn){
	echo "<div class=\"well m-b-30\"><h2>Available Points: {$gd['points']}</h2></div>";
} else {
	echo "<div class=\"well m-b-30\"><h2>You must be signed in to represent a streamer</h2></div>";
};
?>
<div class="col-lg-6 ">
  <div class="panel panel-primary text-center m-t-20">
    <div class="panel-heading">
      <h2 class="panel-title">Represent Streamer</h2>
    </div>
    <div class="panel-body">
    	<div class="row">
          <div class="col-sm-12 col-xs-12">
            <form id="representForm" method="POST" action="">
            	<div class="form-group">
                	<label for="representTarget">Streamer Name *</label>
                    <div class="input-group"><span id="addon2" class="input-group-addon">@</span>
                        <input id="representTarget" type="text" class="form-control" required="" placeholder="Streamer Name *" aria-describedby="addon2">
                        <span class="input-group-btn">
                        <button type="submit" class="btn btn-primary" type="button">
                        Represent!
                        </button>
                        </span> 
                    </div>
                </div>
            </form>
            <p>Representing a streamer will give them points when you earn points! This is a great way to boost a stream's points without actually spending points!</p>
						<p class="text-muted">(*) Please make sure you enter the streamer's name <u>exactly</u>.</em></p>
					</div>
        </div>
    </div>
  </div>
</div>
<div class="col-lg-6 ">
	<div class="panel panel-info text-center m-t-20">
    <div class="panel-heading">
      <h2 class="panel-title">Point Donation</h2>
    </div>
    <div class="panel-body">
    	<div class="row">
          <div class="col-sm-12 col-xs-12">
            <form id="donateForm" method="POST" action="">
            	<div class="form-group">
                	<label for="donateTarget">Streamer Name</label>
                    <div class="input-group"><span id="addon2" class="input-group-addon">@</span>
                        <input id="donateTarget" type="text" class="form-control" required="" placeholder="Streamer Name" aria-describedby="addon2">
                    </div>
                </div>
                <div class="form-group">
                	<label for="donateAmt">Donation Amount</label>
                    <div class="input-group"><span id="addon2" class="input-group-addon"><i class="fa fa-money"></i></span>
                        <input id="donateAmt" type="number" class="form-control" required="" placeholder="Donation Amount (points)" aria-describedby="addon2">
                    </div>
                </div>
                <button class="btn btn-success btn-block waves-effect waves-light m-r-10" type="submit">Donate!</button>
            </form>
            <p class="m-t-20">Direct point donation are a great way to instantly boost a stream's rank. Point donations WILL take from your saved points, but are immediately given to the target stream.</p>
          </div>
        </div>
    </div>
  </div>
</div>

</div>
<!-- /.container-fluid -->
<?php
$noGame = true;
include __DIR__ . '/javascripts.php'; 
include __DIR__ . '/pageClose.php'; 
?>