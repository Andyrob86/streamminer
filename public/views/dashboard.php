<?php
// $loggedIn is a boolean for user logged in state
// $homeUrl is local / remote specific URL var. Also var homeUrl in JS.
$headTitle = "Stream Miner | Dashboard"; //default - Used in <title> tag
$pageTitle = "Dashboard"; //default - Used on pages (breadcrumbs, and page titles)
// $leaderboardEnabled = true; // Enable if leaderboards are present on page
$noGame = true; // set to true if game will not be present on page
// $noMenus = true; // Disables sidebar and top nav
// $daily_leader // is an array with containing the id and name of the highest ranked online stream from the daily stream leaderboard
// $smh variable gives access to Stream Miner function class
// $smh->pageTitle($pageTitle); // use this after opening container to create page title and breadcrumbs.

// $themePlguins is an array that will activate specific theme plugins' CSS and JS based on values in array.
/* 
Available theme plugins: 
		animateBg, bsDatePicker, bsDateRangePicker, bsRTLMaster, bsSelect, bsSocial, bsTable, bsTagsInput, bsTouchSpin, 
		bsTreeView, calendar, chart.js, chartist-js, clockpicker, colorpicker, counterup, cropper, css-chart, custom-select, dataTables, dataTables-Plugin, 
		dataTables-responsive, dropify, dropZone, etc, fancyBox, flot, flot.tooltip, footable, gallery, gmaps, holderjs, horizontal-timeline, html5-editor,
		ion-rangeslider, jquery, jquery.easy-pie-chart, jquery-asColorPicker, jquery-dataTables-editable, jquery-sparkline, jquery-wizard, jsgrind, magnific-popup,
		magnific-popup-master, mjolnic-bootstrap-colorpicker, mocha, moment, morrisjs, multiselect, nestable, owl.carousel, peity, raphael, sidebar-nav, skycons,
		styleSwitcher, summerNote, sweetAlert, switchery, tablesaw, timepicker, tiny-editable, tinymce, toast, typehead.js, vectormap, waypoints, x-editable

 */
$themePlugins = array("sweetAlerts","dataTables"); 

include __DIR__ . '/headerOpen.php'; 
//this area is between <head> and </head>
include __DIR__ . '/headerClose_Nav.php';

?>
<div class="container-fluid">
	<?php $smh->pageTitle($pageTitle); 
	if ($loggedIn) { 
	$user = $smh->getDashboardData($_SESSION['gd']['player']);
	?>
		<div class="row">
			<div class="col-md-6 col-xs-12">
				<div class="white-box">
					<div class="user-bg"> <img alt="user" src="<?php echo $homeUrl; ?>assets/img/profile_bg.jpg" width="100%">
						<div class="overlay-box">
							<div class="user-content"> <a href="javascript:void(0)"><img src="<?php echo $homeUrl; ?>assets/img/logo60.png" class="thumb-lg img-circle" alt="img"></a>
								<h2 class="text-white"><?php echo (isset($user['name']))?$user['name']:"Not Found"; ?></h2>
							</div>
						</div>
					</div>
					<div class="user-btm-box">
						<div class="col-sm-6 text-center">
							<p class="text-purple">Current Daily Score</p>
							<h1><?php echo (isset($user['dailyPoints']))?$user['dailyPoints']:"No Data"; ?></h1>
						</div>
						<div class="col-sm-6 text-center">
							<p class="text-blue">All Time Points</p>
							<h1><?php echo (isset($user['allTimePoints']))?$user['allTimePoints']:"No Data"; ?></h1>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6 col-xs-12">
				<div class="white-box">
					<div class="row">
						<div class="col-md-4 col-xs-6 b-r"> <strong>Email Address</strong> <br>
							<p class="text-muted"><?php echo $user['email']; ?></p>
						</div>
						<div class="col-md-4 col-xs-6 b-r"> <strong>User Since</strong> <br>
							<p class="text-muted"><?php echo $user['joined']; ?></p>
						</div>
						<div class="col-md-4 col-xs-6 b-r"> <strong>Current Tool</strong> <br>
							<p class="text-muted"><?php echo $user['toolName']; ?></p>
						</div>
					</div>
					<hr class="enhancedHr">
					<h4 class="font-bold m-t-30">Stats</h4>
					<hr>
					<p>Detailed Stats coming soon!</p>
					<hr class="enhancedHr m-t-20">
					<h4 class="font-bold m-t-30">Other</h4>
					<hr>
					<form class="form-horizontal form-material" id="changePassForm" action="">
						<h3 class="box-title m-b-20">Change Password</h3>
						<div class="form-group ">
							<div class="col-xs-12">
								<input class="form-control" id="currentPass" type="password" required="" placeholder="Current Password">
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<input class="form-control" id="newpass" type="password" required="" placeholder="New Password">
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<input class="form-control" id="newpass2" type="password" required="" placeholder="Confirm New Password">
							</div>
						</div>
						<input type="hidden" id="token" value="<?php echo $user['token']; ?>" />
						<div class="form-group text-center m-t-20">
							<div class="col-xs-12">
								<button class="btn btn-info btn-lg text-uppercase waves-effect waves-light" type="submit">Change Password</button>
								
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	<?php } else { ?>
		<div class="row">
			<div class="col-md-6 col-md-offset-3 col-xs-12">
				<div class="white-box text-center">
					<h2>You must log in first to view your dashboard!</h2>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 col-xs-12">
				<div class="white-box">
					<form class="form-horizontal form-material" id="loginform" action="">
						<h3 class="box-title m-b-20">Sign In</h3>
						<div class="form-group ">
							<div class="col-xs-12">
								<input class="form-control" id="signInEmail" type="text" required="" placeholder="Email">
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<input class="form-control" id="signInPass" type="password" required="" placeholder="Password">
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="checkbox checkbox-primary pull-left p-t-0">
									<input id="rememberMeCheck" name="rememberMeCheck" type="checkbox">
									<label for="rememberMeCheck"> Remember me </label>
								</div>
								<a href="javascript:void(0)" id="to-recover" class="text-dark pull-right"><i class="fa fa-lock m-r-5"></i> Forgot pwd?</a> </div>
						</div>
						<input type="hidden" id="loginRedirect" value="<?php echo $currentPage; ?>" />
						<div class="form-group text-center m-t-20">
							<div class="col-xs-12">
								<button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Log In</button>
								
							</div>
						</div>
						<div class="form-group m-b-10">
							<div class="col-sm-12 text-center">
								<p>Don't have an account? <a href="<?php echo $homeUrl; ?>register" class="text-primary m-l-5"><b>Sign Up</b></a></p>
							</div>
						</div>
					</form>
					<form class="form-horizontal" id="recoverform" action="">
						<div class="form-group ">
							<div class="col-xs-12">
								<h3>Recover Password</h3>
								<p class="text-muted">Enter your Email and a password reset link well be sent to you! </p>
							</div>
						</div>
						<div class="form-group ">
							<div class="col-xs-12">
								<input class="form-control" id="recoverEmail" type="text" required="" placeholder="Email">
							</div>
						</div>
						<div class="form-group text-center m-t-20">
							<div class="col-xs-12">
								<button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Send Link</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="col-md-6 col-xs-12">
				<div class="white-box">
					<form class="form-horizontal form-material" method="POST" id="registerForm" action="">
						<h3 class="box-title m-b-20">Create an Account</h3>
						<div class="form-group ">
							<div class="col-xs-12">
								<input class="form-control" id="registerUserName" type="text" required="" placeholder="Username">
							</div>
						</div>
						<div class="form-group ">
							<div class="col-xs-12">
								<input class="form-control" id="registerEmail" type="text" required="" placeholder="Email">
							</div>
						</div>
						<div class="form-group ">
							<div class="col-xs-12">
								<input class="form-control" id="registerPass" type="password" required="" placeholder="Password">
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<input class="form-control" id="registerPass2" type="password" required="" placeholder="Confirm Password">
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="checkbox checkbox-primary p-t-0">
									<input id="checkbox-signup" required="" type="checkbox">
									<label for="checkbox-signup"> I agree to all <a href="#">Terms</a></label>
								</div>
							</div>
						</div>
						<div class="form-group text-center m-t-20">
							<div class="col-xs-12">
								<button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Sign Up</button>
							</div>
						</div>
						<div class="form-group m-b-0">
							<div class="col-sm-12 text-center">
								<p>Already have an account? <a href="<?php echo $homeUrl; ?>login" class="text-primary m-l-5"><b>Sign In</b></a></p>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
<!-- /.container-fluid -->
<?php
};
include __DIR__ . '/javascripts.php'; 
include __DIR__ . '/pageClose.php'; 
?>