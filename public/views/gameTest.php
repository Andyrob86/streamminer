<?php
// $loggedIn is a boolean for user logged in state
// $homeUrl is local / remote specific URL var. Also var homeUrl in JS.
// $leaderboardEnabled = true; // Enable if leaderboards are present on page
// $noGame = true; // set to true if game will not be present on page
// $noMenus = true; // Disables sidebar and top nav
// $daily_leader will give current daily leader! surprise!
$headTitle = "Stream Miner | Game Test";

include __DIR__ . '/headerOpen.php'; 
?>

<?php
include __DIR__ . '/headerClose_Nav.php';

?>    
<div class="container-fluid orbitron">
	<div class="row">
		<div class="col-lg-10 col-lg-offset-1">
			<div class="white box">
				<div id="gameDiv" style="height:800px;"></div>
				<?php if(!$loggedIn){ ?><div class="alert alert-danger">Must be logged in to save progress!</div><?php } ?>
			</div>
		</div>
	</div>
	
</div>
<!-- /.container-fluid -->
<?php
include __DIR__ . '/javascripts.php';
?>
<script type="text/javascript">
	$(document).ready(function(){
		<?php 
		if(isset($tool)){
			echo "var toolNum = $tool;";
		};
		?>
		gameStart();
	})
</script>
<?php
include __DIR__ . '/pageClose.php'; 
?>