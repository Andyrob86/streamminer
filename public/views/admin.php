<?php
// $loggedIn is a boolean for user logged in state
// $homeUrl is local / remote specific URL var. Also var homeUrl in JS.
$headTitle = "Stream Miner | Admin"; //default - Used in <title> tag
$pageTitle = "Admin Dashboard"; //default - Used on pages (breadcrumbs, and page titles)
$leaderboardEnabled = true; // Enable if leaderboards are present on page
$noGame = true; // set to true if game will not be present on page
// $noMenus = true; // Disables sidebar and top nav
// $daily_leader // is an array with containing the id and name of the highest ranked online stream from the daily stream leaderboard
// $smh variable gives access to Stream Miner function class
// $smh->pageTitle($pageTitle); // use this after opening container to create page title and breadcrumbs.

// $themePlguins is an array that will activate specific theme plugins' CSS and JS based on values in array.
/* 
Available theme plugins: 
		animateBg, bsDatePicker, bsDateRangePicker, bsRTLMaster, bsSelect, bsSocial, bsTable, bsTagsInput, bsTouchSpin, 
		bsTreeView, calendar, chart.js, chartist-js, clockpicker, colorpicker, counterup, cropper, css-chart, custom-select, dataTables, dataTables-Plugin, 
		dataTables-responsive, dropify, dropZone, etc, fancyBox, flot, flot.tooltip, footable, gallery, gmaps, holderjs, horizontal-timeline, html5-editor,
		ion-rangeslider, jquery, jquery.easy-pie-chart, jquery-asColorPicker, jquery-dataTables-editable, jquery-sparkline, jquery-wizard, jsgrind, magnific-popup,
		magnific-popup-master, mjolnic-bootstrap-colorpicker, mocha, moment, morrisjs, multiselect, nestable, owl.carousel, peity, raphael, sidebar-nav, skycons,
		styleSwitcher, summerNote, sweetAlert, switchery, tablesaw, timepicker, tiny-editable, tinymce, toast, typehead.js, vectormap, waypoints, x-editable

 */
$themePlugins = array("sweetAlerts","dataTables"); 

include __DIR__ . '/headerOpen.php'; 
//this area is between <head> and </head>
include __DIR__ . '/headerClose_Nav.php';

?>    
<div class="container-fluid">
	<?php $smh->pageTitle($pageTitle); ?>
	
</div>
<!-- /.container-fluid -->
<?php
include __DIR__ . '/javascripts.php'; 
include __DIR__ . '/pageClose.php'; 
?>