<?php 
$noMenus = true;
$headTitle = "Stream Miner | Resend Activation Key";
$themePlugins = array("sweetAlerts"); 
include __DIR__ . '/headerOpen.php'; 
?>

<?php
include __DIR__ . '/headerClose_Nav.php'; 
 ?>
	<section id="wrapper" class="login-register orbitron">
		<div id="scrollDiv">
			<div class="login-box">
				<a href="<?php echo $homeUrl; ?>"><img src="<?php echo $homeUrl;  ?>assets/img/TextLogo500.png" class="img-responsive" alt="Stream Miner" /></a>
				<div class="white-box">
					<form class="form-horizontal form-material" method="POST" id="resendKeyForm" action="">
						<h3 class="box-title m-b-20">Resend Activation Key</h3>
						<p>Enter your email address to have a new activation key sent.</p>
						<div class="form-group ">
							<div class="col-xs-12">
								<input class="form-control" id="resendKeyEmail" type="text" required="" placeholder="Email Address">
							</div>
						</div>
						<div class="form-group text-center m-t-20">
							<div class="col-xs-12">
								<button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Resend Key</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
	
<?php

$noGame = true;
include __DIR__ . '/javascripts.php';
?>
<script type="text/javascript">
if($(".login-box").height() > $(window).height()){
	$('#scrollDiv').slimScroll({
			height: $(window).height() + "px"
	});
}
</script>
<?php
include __DIR__ . '/pageClose.php'; 
?>