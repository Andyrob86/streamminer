<!-- jQuery -->
<script src="<?php echo $homeUrl; ?>plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo $homeUrl; ?>assets/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Sidebar menu plugin JavaScript -->
<script src="<?php echo $homeUrl; ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--Slimscroll JavaScript For custom scroll-->
<script src="<?php echo $homeUrl; ?>assets/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo $homeUrl; ?>assets/js/waves.js"></script>
<!--Plugin switch -->
<?php require_once '../src/includes/plugin-js-switch.php'; ?>
<!-- Custom Theme JavaScript -->

<script src="<?php echo $homeUrl; ?>assets/js/custom.min.js"></script>
<?php
if($leaderboardEnabled){ ?>
	<script src="<?php echo $homeUrl; ?>plugins/bower_components/datatables/jquery.dataTables.min.js"></script>
<?php };
if(!isset($noGame)){ ?>
	<script type="text/javascript" src="<?php echo $homeUrl; ?>assets/js/phaser.js"></script>
	<script type="text/javascript" src="<?php echo $homeUrl; ?>assets/js/game.js"></script>
<?php } ?>
<!--Style Switcher -->
<script src="<?php echo $homeUrl; ?>plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
<script src="<?php echo $homeUrl; ?>assets/js/page.js"></script>