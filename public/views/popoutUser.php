<?php 
$pageTitle = "Stream Miner | $user";
$noMenus = true;
include __DIR__ . '/headerOpen.php'; 
?>

<?php
include __DIR__ . '/headerClose_Nav.php'; 
?>
<div class="container-fluid">
		<div class="row">
				<div class="col-lg-12 no-padding">
						<script src= "https://player.twitch.tv/js/embed/v1.js"></script>
						<div class="embed-responsive embed-responsive-16by9" id="streamPlayer" data-stream="<?php echo $user; ?>">
						<script type="text/javascript">
								var options = {
										channel: "<?php echo $user; ?>", 
										//video: "{VIDEO_ID}"       
								};
								var player = new Twitch.Player("streamPlayer", options);
								player.setVolume(0.5);
								player.addEventListener(Twitch.Player.PAUSE, () => { console.log('Player is paused!'); });
						</script>
					</div>
				</div>
		</div>
		<!-- /.row -->
</div>
<!-- /.container-fluid -->
<?php
$noGame = true;
include __DIR__ . '/javascripts.php'; 
include __DIR__ . '/pageClose.php'; 
 ?>