<?php
// $loggedIn is a boolean for user logged in state
// $homeUrl is local / remote specific URL var. Also var homeUrl in JS.
$headTitle = "Stream Miner | Leaderboards"; //default
// $leaderboardEnabled = true; // Enable if leaderboards are present on page
// $noGame = true; // set to true if game will not be present on page
// $noMenus = true; // Disables sidebar and top nav
// $daily_leader will give current daily stream leader! surprise!

include __DIR__ . '/headerOpen.php'; 
//this area is between <head> and </head>
include __DIR__ . '/headerClose_Nav.php';

?>
    
<div class="container-fluid">
<?php 
$pageTitle = ($leaderboardPage == "streams")?"Streamer Leaderboards":"Player Leaderboards";
$smh->pageTitle($pageTitle); 
?>
	<div class="row">
    <?php 
		$leaderboardPage = (isset($leaderboardPage))?$leaderboardPage:"";
		$lbActiveTab = (isset($lbActiveTab))?($lbActiveTab == "")?"daily":$lbActiveTab:"daily";
		//echo $lbActiveTab;
		switch($leaderboardPage){
			case "streams": ?>
				<div class="col-lg-8 col-lg-offset-2">
                	<div class="white-box">
					<ul class="nav nav-tabs customtab">
						<li class="<?php echo ($lbActiveTab == "daily")?"active":""; ?>"><a class="leaderTab" data-crumbPage="Daily" href="#streamDaily" data-toggle="tab">Daily</a>
						</li>
						<li class="<?php echo ($lbActiveTab == "weekly")?"active":""; ?>"><a class="leaderTab" data-crumbPage="Weekly" href="#streamWeekly" data-toggle="tab">Weekly</a>
						</li>
						<li class="<?php echo ($lbActiveTab == "monthly")?"active":""; ?>"><a class="leaderTab" data-crumbPage="Monthly" href="#streamMonthly" data-toggle="tab">Monthly</a>
						</li>
						<li class="<?php echo ($lbActiveTab == "alltime")?"active":""; ?>"><a class="leaderTab" data-crumbPage="All-Time" href="#streamAllTime" data-toggle="tab">All-Time</a>
						</li>
					</ul>
			
					<!-- Tab panes -->
					<div class="tab-content m-t-0">
						<div class="tab-pane fade <?php echo ($lbActiveTab == "daily")?"in active":""; ?>" id="streamDaily">
							<?php include '../src/includes/leaderboard_stream_daily.php'; ?>
						</div>
						<div class="tab-pane fade <?php echo ($lbActiveTab == "weekly")?"in active":""; ?>" id="streamWeekly">
							<?php include '../src/includes/leaderboard_stream_weekly.php'; ?>
						</div>
						<div class="tab-pane fade <?php echo ($lbActiveTab == "monthly")?"in active":""; ?>" id="streamMonthly">
							<?php include '../src/includes/leaderboard_stream_monthly.php'; ?>
						</div>
						<div class="tab-pane fade <?php echo ($lbActiveTab == "alltime")?"in active":""; ?>" id="streamAllTime">
							<?php include '../src/includes/leaderboard_stream_all_time.php'; ?>
						</div>
					</div>
                   </div>
				</div>
         	 	<?php break;
			case "players": ?>
				<div class="col-lg-8 col-lg-offset-2">
                	<div class="white-box">
					<ul class="nav nav-tabs top-space2x">
						<li class="<?php echo ($lbActiveTab == "daily")?"active":""; ?>"><a class="leaderTab" data-crumbPage="Daily" href="#playerDaily" data-toggle="tab">Daily</a>
						</li>
						<li class="<?php echo ($lbActiveTab == "weekly")?"active":""; ?>"><a class="leaderTab" data-crumbPage="Weekly" href="#playerWeekly" data-toggle="tab">Weekly</a>
						</li>
						<li class="<?php echo ($lbActiveTab == "monthly")?"active":""; ?>"><a class="leaderTab" data-crumbPage="Monthly" href="#playerMonthly" data-toggle="tab">Monthly</a>
						</li>
						<li class="<?php echo ($lbActiveTab == "alltime")?"active":""; ?>"><a class="leaderTab" data-crumbPage="All-Time" href="#playerAllTime" data-toggle="tab">All-Time</a>
						</li>
					</ul>
			
					<!-- Tab panes -->
					<div class="tab-content m-t-0">
						<div class="tab-pane fade <?php echo ($lbActiveTab == "daily")?"in active":""; ?>" id="playerDaily">
							<?php include '../src/includes/leaderboard_player_daily.php'; ?>
						</div>
						<div class="tab-pane fade <?php echo ($lbActiveTab == "weekly")?"in active":""; ?>" id="playerWeekly">
							<?php include '../src/includes/leaderboard_player_weekly.php'; ?>
						</div>
						<div class="tab-pane fade <?php echo ($lbActiveTab == "monthly")?"in active":""; ?>" id="playerMonthly">
							<?php include '../src/includes/leaderboard_player_monthly.php'; ?>
						</div>
						<div class="tab-pane fade <?php echo ($lbActiveTab == "alltime")?"in active":""; ?>" id="playerAllTime">
							<?php include '../src/includes/leaderboard_player_all_time.php'; ?>
						</div>
					</div>
                   </div>
				</div>
         	 	<?php break;
			case "leaderboardSearch": ?>
				<div class="col-lg-4 col-lg-offset-4">
					<div class="panel panel-warning text-center top-space2x">
						<div class="panel-heading">
							<h2 class="panel-title">Leaderboard Search</h2>
						</div>
						<div class="panel-body">
							<h2>Coming Soon!</h2>
							<p>Detailed stats about specifc streams or players (if they opt to show stats!) will be coming soon!</p>
						</div>
					</div>
				</div> 
         
       <?php break;
	   	}; ?>
    </div>
</div>
<!-- /.container-fluid -->
<?php
include __DIR__ . '/javascripts.php'; 
include __DIR__ . '/pageClose.php'; 
?>