<?php 
$pageTitle = "Stream Miner | $user";
include __DIR__ . '/headerOpen.php'; 
?>

<?php
include __DIR__ . '/headerClose_Nav.php'; 
?>
<div class="container-fluid">
<?php $smh->pageTitle($pageTitle, $user); ?>
		<div class="row a-p">
				<div class="col-lg-9 no-padding">
						<script src= "https://player.twitch.tv/js/embed/v1.js"></script>
						<div class="embed-responsive embed-responsive-16by9" id="streamPlayer" data-stream="<?php echo $user; ?>">
						<script type="text/javascript">
								var options = {
										channel: "<?php echo $user; ?>", 
										//video: "{VIDEO_ID}"       
								};
								var player = new Twitch.Player("streamPlayer", options);
								player.setVolume(0.5);
								player.addEventListener(Twitch.Player.PAUSE, () => { console.log('Player is paused!'); });
						</script>
					</div>
					<div id="chatDiv" data-chat="<?php echo $user; ?>">
						<iframe
							frameborder="0" 
							scrolling="no" 
							id="chat_embed" 
							src="https://www.twitch.tv/<?php echo $user; ?>/chat" 
							height="450" 
							width="100%">
						</iframe>
					</div>
				</div>
				<div class="col-lg-3 no-padding">
				<div id="gameDiv"></div>
				<?php if(!$loggedIn){ ?><div class="alert alert-danger">Must be logged in to save progress!</div><?php };
				if (isset($_SESSION['gd']['representing'])){
					$stream = $_SESSION['gd']['representing'];
					echo "<div class=\"alert alert-info\">Representing: <a href=\"{$homeUrl}/u/$stream\" style=\"color:white\">$stream</a></div>";
				};
				include '../src/includes/leaderboard_stream_top_100.php'; ?>
				</div>
		</div>
		<!-- /.row -->
</div>
<!-- /.container-fluid -->
<?php 
include __DIR__ . '/javascripts.php';
?>
<script type="text/javascript">
	$(document).ready(function(){
		gameStart();
	})
</script>
<?php 
include __DIR__ . '/pageClose.php'; 
 ?>