<?php
// $loggedIn is a boolean for user logged in state
// $homeUrl is local / remote specific URL var. Also var homeUrl in JS.
$headTitle = "Stream Miner | About"; //default - Used in <title> tag
$pageTitle = "About"; //default - Used on pages (breadcrumbs, and page titles)
// $leaderboardEnabled = true; // Enable if leaderboards are present on page
// $noGame = true; // set to true if game will not be present on page
// $noMenus = true; // Disables sidebar and top nav
// $daily_leader // is an array with containing the id and name of the highest ranked online stream from the daily stream leaderboard
// $smh variable gives access to Stream Miner function class
// $smh->pageTitle($pageTitle); // use this after opening container to create page title and breadcrumbs.

// $themePlguins is an array that will activate specific theme plugins' CSS and JS based on values in array.
/* 
Available theme plugins: 
		animateBg, bsDatePicker, bsDateRangePicker, bsRTLMaster, bsSelect, bsSocial, bsTable, bsTagsInput, bsTouchSpin, 
		bsTreeView, calendar, chart.js, chartist-js, clockpicker, colorpicker, counterup, cropper, css-chart, custom-select, dataTables, dataTables-Plugin, 
		dataTables-responsive, dropify, dropZone, etc, fancyBox, flot, flot.tooltip, footable, gallery, gmaps, holderjs, horizontal-timeline, html5-editor,
		ion-rangeslider, jquery, jquery.easy-pie-chart, jquery-asColorPicker, jquery-dataTables-editable, jquery-sparkline, jquery-wizard, jsgrind, magnific-popup,
		magnific-popup-master, mjolnic-bootstrap-colorpicker, mocha, moment, morrisjs, multiselect, nestable, owl.carousel, peity, raphael, sidebar-nav, skycons,
		styleSwitcher, summerNote, sweetAlert, switchery, tablesaw, timepicker, tiny-editable, tinymce, toast, typehead.js, vectormap, waypoints, x-editable

 */
$themePlugins = array("sweetAlerts","dataTables"); 

include __DIR__ . '/headerOpen.php'; 
//this area is between <head> and </head>
include __DIR__ . '/headerClose_Nav.php';

?>    
<div class="container-fluid">
	<?php $smh->pageTitle($pageTitle); ?>
	<div class="row">
		<div class="col-md-6 col-md-offset-3 col-xs-12">
			<div class="white-box text-center">
				<h2>What is</h2>
				<img src="<?php echo $homeUrl;  ?>assets/img/TextLogo500.png" class="img-responsive " style="margin:0 auto;" alt="Stream Miner" />
				<h2>Anyway?</h2>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-8 col-xs-12">
			<div class="white-box">
				<p>Stream Miner is a game you can play while watching your favorite stream, while representing your favorite stream at the same time! The game is pretty simple to play, you just mine for points while you watch! use those points to purchase upgrades, climb the leaderboards, or represent your favorite streamer.</p>
				<p>The homepage of <a href="<?php echo $homeUrl; ?>">Stream Miner</a> will display the highest ranked stream for the day based on the players "representing" them.</p>
				<p>Players can choose to represent a streamer, which will give the streamer 10% of all points earned by the player while they play (Don't worry! They don't <b>TAKE</b> the points from you, they just earn 10% on top of what you earn!).</p>
				<p>Players can also choose to "donate" their daily leaderboard points to their favorite streamer. Donations <b>DO</b> take points from your available points, and the daily leaderboards! So while they are a great way to boost a streamer, they will reduce your rank on the player leaderboards, so donate wisely!</p>
			</div>
		</div>
		<div class="col-md-4 col-xs-12">
			<div id="gameDiv"></div>
			<?php if(!$loggedIn){ ?><div class="alert alert-danger">Must be logged in to save progress!</div><?php } ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6 col-md-offset-3 col-xs-12">
			<p>Stream Miner is brand new, but we want to make sure you have fun, and if you run into any problems, we want to help! Please don't hesitate to contact us at <a href="mailto:support@streamminer.tv">support@streamminer.tv</a> with any questions, or suggestions you may have!.</p>
		</div>
	</div>
	

</div>
<!-- /.container-fluid -->
<?php
include __DIR__ . '/javascripts.php';
?>
<script type="text/javascript">
	gameStart();
</script>
<?php
include __DIR__ . '/pageClose.php'; 
?>