<?php 
$noMenus = true;
$headTitle = "Stream Miner | Activate Account";
$themePlugins = array("sweetAlerts"); 
include __DIR__ . '/headerOpen.php'; 
?>

<?php
include __DIR__ . '/headerClose_Nav.php'; 

if (!isset($token)){ ?>
	<section id="wrapper" class="login-register orbitron">
		<div id="scrollDiv">
			<div class="login-box">
				<a href="<?php echo $homeUrl; ?>"><img src="<?php echo $homeUrl;  ?>assets/img/TextLogo500.png" class="img-responsive" alt="Stream Miner" /></a>
				<div class="white-box">
					<form class="form-horizontal form-material" method="POST" id="activateForm" action="">
						<h3 class="box-title m-b-20">Activate Account</h3>
						<p>Enter your activation key below to activate your account on Stream Miner.</p>
						<div class="form-group ">
							<div class="col-xs-12">
								<input class="form-control" id="activationKey" type="text" required="" placeholder="Activation Key">
							</div>
						</div>
						<div class="form-group text-center m-t-20">
							<div class="col-xs-12">
								<button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Activate Account</button>
							</div>
						</div>
						<p class="text-muted">If your key has expired, you can <a href="<?php echo $homeUrl; ?>resendActivation">send a new activation key</a>.</p>
					</form>
				</div>
			</div>
		</div>
	</section>
<?php } else { 
	$response = $auth->activate($token);
	$responseClass = ($response['error'])?"danger":"success";
	?>
	<section id="wrapper" class="login-register">
		<div id="scrollDiv">
			<div class="login-box">
				<a href="<?php echo $homeUrl; ?>"><img src="<?php echo $homeUrl;  ?>assets/img/TextLogo500.png" class="img-responsive" alt="Stream Miner" /></a>
				<div class="panel panel-<?php echo $responseClass; ?> m-t-20">
					<div class="panel-heading"> Activate Account</div>
					<div class="panel-wrapper collapse in" aria-expanded="true">
						<div class="panel-body">
							<p><?php echo $response['message']; ?></p>
							<?php echo (!$response['error'])?"<a href=\"{$homeUrl}login\"><button class=\"btn btn-success m-t-10\">To Login</button></a>":"Please double check the link, or try activating by visting <a href=\"{$homeUrl}activate\">streamminer.tv/activate</a> and entering your activation key."; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php };

$noGame = true;
include __DIR__ . '/javascripts.php';
?>
<script type="text/javascript">
if($(".login-box").height() > $(window).height()){
	$('#scrollDiv').slimScroll({
			height: $(window).height() + "px"
	});
}
</script>
<?php
include __DIR__ . '/pageClose.php'; 
?>