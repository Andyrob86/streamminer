<?php 
$noMenus = true;
$headTitle = "Stream Miner | Register";
$themePlugins = array("sweetAlerts"); 
include __DIR__ . '/headerOpen.php';
?>

<?php
include __DIR__ . '/headerClose_Nav.php'; 
?>
<section id="wrapper" class="login-register orbitron">
  <div id="scrollDiv">
	<div class="login-box">
		<a href="<?php echo $homeUrl; ?>"><img src="<?php echo $homeUrl;  ?>assets/img/TextLogo500.png" class="img-responsive" alt="Stream Miner" /></a>
		<div class="white-box">
      <form class="form-horizontal form-material" method="POST" id="registerForm" action="">
        <h3 class="box-title m-b-20">Create an Account</h3>
        <div class="form-group ">
          <div class="col-xs-12">
            <input class="form-control" id="registerUserName" type="text" required="" placeholder="Username">
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-12">
            <input class="form-control" id="registerEmail" type="text" required="" placeholder="Email">
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-12">
            <input class="form-control" id="registerPass" type="password" required="" placeholder="Password">
          </div>
        </div>
        <div class="form-group">
          <div class="col-xs-12">
            <input class="form-control" id="registerPass2" type="password" required="" placeholder="Confirm Password">
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <div class="checkbox checkbox-primary p-t-0">
              <input id="checkbox-signup" required="" type="checkbox">
              <label for="checkbox-signup"> I agree to all <a href="<?php echo $homeUrl; ?>terms" target="_blank">Terms</a></label>
            </div>
          </div>
        </div>
        <div class="form-group text-center m-t-20">
          <div class="col-xs-12">
            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Sign Up</button>
          </div>
        </div>
        <div class="form-group m-b-0">
          <div class="col-sm-12 text-center">
            <p>Already have an account? <a href="<?php echo $homeUrl; ?>login" class="text-primary m-l-5"><b>Sign In</b></a></p>
          </div>
        </div>
      </form>
			</div>
    </div>
  </div>
</section>
<?php 
$noGame = true;
include __DIR__ . '/javascripts.php';
?>
<script type="text/javascript">
if($(".login-box").height() > $(window).height()){
	$('#scrollDiv').slimScroll({
			height: $(window).height() + "px"
	});
}
</script>
<?php
include __DIR__ . '/pageClose.php'; 
?>
