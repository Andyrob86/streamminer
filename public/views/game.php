<?php 
$headTitle = "Stream Miner | Game";
$pageTitle = "Game Only";
// $noMenus = true;
include __DIR__ . '/headerOpen.php'; 
?>

<?php
include __DIR__ . '/headerClose_Nav.php'; 
?>
<div class="container-fluid orbitron">
		<div class="row a-p">
				<div class="col-lg-6 no-padding">
					<div id="gameDiv"></div>
					<?php if(!$loggedIn){ ?><div class="alert alert-danger">Must be logged in to save progress!</div><?php };
					if (isset($_SESSION['gd']['representing'])){
						$stream = $_SESSION['gd']['representing'];
						echo "<div class=\"alert alert-info\">Representing: <a href=\"{$homeUrl}/u/$stream\" style=\"color:white\">$stream</a></div>";
					};
					?>
				</div>
				<div class="col-lg-6 no-padding">
				<?php include '../src/includes/leaderboard_stream_daily.php'; ?>
				</div>
		</div>
		<!-- /.row -->
</div>
<!-- /.container-fluid -->
<?php
include __DIR__ . '/javascripts.php'; 
?>
<script type="text/javascript">
	gameStart();
</script>
<?php
include __DIR__ . '/pageClose.php'; 
 ?>