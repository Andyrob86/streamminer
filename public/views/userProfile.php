<?php
// $loggedIn is a boolean for user logged in state
// $homeUrl is local / remote specific URL var. Also var homeUrl in JS.
$headTitle = (isset($user['name']))?"Stream Miner | {$user['name']}":"Stream Miner | Profile"; //default - Used in <title> tag
$pageTitle = (isset($user))?$user['name']:"Profile"; //default - Used on pages (breadcrumbs, and page titles)
// $leaderboardEnabled = true; // Enable if leaderboards are present on page
$noGame = true; // set to true if game will not be present on page
// $noMenus = true; // Disables sidebar and top nav
// $daily_leader // is an array with containing the id and name of the highest ranked online stream from the daily stream leaderboard
// $smh variable gives access to Stream Miner function class
// $smh->pageTitle($pageTitle); // use this after opening container to create page title and breadcrumbs.

// $themePlguins is an array that will activate specific theme plugins' CSS and JS based on values in array.
/* 
Available theme plugins: 
		animateBg, bsDatePicker, bsDateRangePicker, bsRTLMaster, bsSelect, bsSocial, bsTable, bsTagsInput, bsTouchSpin, 
		bsTreeView, calendar, chart.js, chartist-js, clockpicker, colorpicker, counterup, cropper, css-chart, custom-select, dataTables, dataTables-Plugin, 
		dataTables-responsive, dropify, dropZone, etc, fancyBox, flot, flot.tooltip, footable, gallery, gmaps, holderjs, horizontal-timeline, html5-editor,
		ion-rangeslider, jquery, jquery.easy-pie-chart, jquery-asColorPicker, jquery-dataTables-editable, jquery-sparkline, jquery-wizard, jsgrind, magnific-popup,
		magnific-popup-master, mjolnic-bootstrap-colorpicker, mocha, moment, morrisjs, multiselect, nestable, owl.carousel, peity, raphael, sidebar-nav, skycons,
		styleSwitcher, summerNote, sweetAlert, switchery, tablesaw, timepicker, tiny-editable, tinymce, toast, typehead.js, vectormap, waypoints, x-editable

 */
$themePlugins = array("sweetAlerts","dataTables"); 

include __DIR__ . '/headerOpen.php'; 
//this area is between <head> and </head>
include __DIR__ . '/headerClose_Nav.php';

?>    
<div class="container-fluid">
	<?php $smh->pageTitle($pageTitle); ?>
	<div class="row">
		<div class="col-md-6 col-md-offset-3 col-xs-12">
				<div class="white-box">
					<div class="user-bg"> <img alt="user" src="<?php echo $homeUrl; ?>assets/img/profile_bg.jpg" width="100%">
						<div class="overlay-box">
							<div class="user-content"> <a href="javascript:void(0)"><img src="<?php echo $homeUrl; ?>assets/img/logo60.png" class="thumb-lg img-circle" alt="img"></a>
								<h2 class="text-white"><?php echo (isset($user['name']))?$user['name']:"Not Found"; ?></h2>
							</div>
						</div>
					</div>
					<div class="user-btm-box">
						<div class="col-sm-6 text-center">
							<p class="text-purple">Current Daily Score</p>
							<h1><?php echo (isset($user['dailyPoints']))?$user['dailyPoints']:"No Data"; ?></h1>
						</div>
						<div class="col-sm-6 text-center">
							<p class="text-blue">All Time Points</p>
							<h1><?php echo (isset($user['allTimePoints']))?$user['allTimePoints']:"No Data"; ?></h1>
						</div>
					</div>
				</div>
			</div>
	</div>
	<div class="row">
		<div class="col-md-6 col-md-offset-3 col-xs-12">
			<div class="white-box">
				<div class="text-center">
					<h2>Stats</h2>
					<p>Coming Soon</p>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /.container-fluid -->
<?php
include __DIR__ . '/javascripts.php'; 
include __DIR__ . '/pageClose.php'; 
?>