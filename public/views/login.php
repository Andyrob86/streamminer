<?php 
$noMenus = true;
$themePlugins = array("sweetAlerts"); 
include __DIR__ . '/headerOpen.php';
?>

<?php
include __DIR__ . '/headerClose_Nav.php'; 
?>
<section id="wrapper" class="login-register orbitron">
	<div id="scrollDiv">
		<div class="login-box">
			<a href="<?php echo $homeUrl; ?>"><img src="<?php echo $homeUrl;  ?>assets/img/TextLogo500.png" class="img-responsive" alt="Stream Miner" /></a>
			<div class="white-box m-t-20">
				<form class="form-horizontal form-material" id="loginform" action="">
					<h3 class="box-title m-b-20">Sign In</h3>
					<div class="form-group ">
						<div class="col-xs-12">
							<input class="form-control" id="signInEmail" type="text" required="" placeholder="Email">
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12">
							<input class="form-control" id="signInPass" type="password" required="" placeholder="Password">
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12">
							<div class="checkbox checkbox-primary pull-left p-t-0">
								<input id="rememberMeCheck" name="rememberMeCheck" type="checkbox">
								<label for="rememberMeCheck"> Remember me </label>
							</div>
							<a href="javascript:void(0)" id="to-recover" class="text-dark pull-right"><i class="fa fa-lock m-r-5"></i> Forgot pwd?</a> </div>
					</div>
					<div class="form-group text-center m-t-20">
						<div class="col-xs-12">
							<button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Log In</button>
							
						</div>
					</div>
					<div class="form-group m-b-10">
						<div class="col-sm-12 text-center">
							<p>Don't have an account? <a href="<?php echo $homeUrl; ?>register" class="text-primary m-l-5"><b>Sign Up</b></a></p>
						</div>
					</div>
				</form>
				<form class="form-horizontal" id="recoverform" action="">
					<div class="form-group ">
						<div class="col-xs-12">
							<h3>Recover Password</h3>
							<p class="text-muted">Enter your Email and a password reset link well be sent to you! </p>
						</div>
					</div>
					<div class="form-group ">
						<div class="col-xs-12">
							<input class="form-control" id="recoverEmail" type="text" required="" placeholder="Email">
						</div>
					</div>
					<div class="form-group text-center m-t-20">
						<div class="col-xs-12">
							<button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Send Link</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>




<?php 
$noGame = true;
include __DIR__ . '/javascripts.php';
?>
<script type="text/javascript">
if($(".login-box").height() > $(window).height()){
	$('#scrollDiv').slimScroll({
			height: $(window).height() + "px"
	});
}
</script>
<?php
include __DIR__ . '/pageClose.php'; 
?>
