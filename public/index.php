<?php
session_start();
include_once '../vendor/autoload.php';
$router = new AltoRouter();

// standard page mapping
$homeURI = ($_SERVER['SERVER_NAME'] == "localhost")?"/streamminer/public/":"/";
$router->map( 'GET', $homeURI, 'homeMapping');
$router->map( 'GET', $homeURI.'home', 'homeMapping');
function homeMapping() {
	require __DIR__ . '/views/home.php';
};

$aboutURI = ($_SERVER['SERVER_NAME'] == "localhost")?"/streamminer/public/about":"/about";
$router->map( 'GET', $aboutURI, function() {
	require __DIR__ . '/views/about.php';
});

$tospp = ($_SERVER['SERVER_NAME'] == "localhost")?"/streamminer/public/tospp":"/tospp";
$terms = ($_SERVER['SERVER_NAME'] == "localhost")?"/streamminer/public/terms":"/terms";
$router->map( 'GET', $tospp, 'termsCombined');
$router->map( 'GET', $terms, 'termsCombined');
function termsCombined() {
	require __DIR__ . '/views/tos_pp.php';
};

$tosShort = ($_SERVER['SERVER_NAME'] == "localhost")?"/streamminer/public/tos":"/tos";
$tosLong = ($_SERVER['SERVER_NAME'] == "localhost")?"/streamminer/public/terms-of-service":"/terms-of-service";
$router->map( 'GET', $tosShort, 'tos');
$router->map( 'GET', $tosLong, 'tos');
function tos() {
	require __DIR__ . '/views/tos.php';
}

$ppShort = ($_SERVER['SERVER_NAME'] == "localhost")?"/streamminer/public/pp":"/pp";
$ppLong = ($_SERVER['SERVER_NAME'] == "localhost")?"/streamminer/public/privacy-policy":"/privacy-policy";
$router->map( 'GET', $ppShort, 'pp');
$router->map( 'GET', $ppLong, 'pp');
function pp() {
	require __DIR__ . '/views/pp.php';
}

$shopURI = ($_SERVER['SERVER_NAME'] == "localhost")?"/streamminer/public/shop":"/shop";
$router->map( 'GET', $shopURI, function() {
	require __DIR__ . '/views/shop.php';
});

$router->map( 'POST', $shopURI, function() {
	require __DIR__ . '/../src/handlers/siteHandler.php';
});

$representURI = ($_SERVER['SERVER_NAME'] == "localhost")?"/streamminer/public/represent":"/represent";
$router->map( 'GET', $representURI, function() {
	require __DIR__ . '/views/represent.php';
});

$router->map( 'POST', $representURI, function() {
	require __DIR__ . '/../src/handlers/siteHandler.php';
});

$loginURI = ($_SERVER['SERVER_NAME'] == "localhost")?"/streamminer/public/login":"/login";
$router->map( 'GET', $loginURI, function() {
	require __DIR__ . '/views/login.php';
});

$router->map( 'POST', $loginURI, function() {
	require __DIR__ . '/../src/handlers/loginHandler.php';
});

$logoutURI = ($_SERVER['SERVER_NAME'] == "localhost")?"/streamminer/public/logout":"/logout";
$router->map( 'GET', $logoutURI, function() {
	require __DIR__ . '/views/logout.php';
});

$timeoutURI = ($_SERVER['SERVER_NAME'] == "localhost")?"/streamminer/public/logout/timeout":"/logout/timeout";
$router->map( 'GET', $timeoutURI, function() {
	$timeoutNotice = true;
	require __DIR__ . '/views/logout.php';
});

$router->map( 'POST', $logoutURI, function() {
	require __DIR__ . '/../src/handlers/loginHandler.php';
});

$registerURI = ($_SERVER['SERVER_NAME'] == "localhost")?"/streamminer/public/register":"/register";
$router->map( 'GET', $registerURI, function() {
	require __DIR__ . '/views/register.php';
});

$resetURI = ($_SERVER['SERVER_NAME'] == "localhost")?"/streamminer/public/reset":"/reset";
$router->map( 'GET', $resetURI, "resetMapping");
$router->map( 'GET', $resetURI."/", "resetMapping");

function resetMapping() {
	require __DIR__ . '/views/resetPass.php';
};

$keyResetURI = ($_SERVER['SERVER_NAME'] == "localhost")?"/streamminer/public/reset/[a:resetkey]":"/reset/[a:resetkey]";
$router->map( 'GET', $keyResetURI, function($resetkey) {
	require __DIR__ . '/views/resetPass.php';
});

$recoverURI = ($_SERVER['SERVER_NAME'] == "localhost")?"/streamminer/public/recoverpass":"/recoverpass";
$router->map( 'GET', $recoverURI, function() {
	require __DIR__ . '/views/recoverPass.php';
});

$testURI = ($_SERVER['SERVER_NAME'] == "localhost")?"/streamminer/public/test":"/test";
$router->map( 'GET', $testURI , function() {
	if(isset($_SESSION['gd']['uid'])){
		$users = new Users();
		$users->find($_SESSION['gd']['uid']);
		if($users->admin == 1){
			require __DIR__ . '/views/test.php';
		} else {
			require __DIR__ . '/views/errors/404.php';
		}
	} else {
		require __DIR__ . '/views/errors/404.php';
	}
	
	
});

$router->map( 'POST', $testURI , function() {
	require __DIR__ . '/views/test.php';
});

$gameTestURI = ($_SERVER['SERVER_NAME'] == "localhost")?"/streamminer/public/gt":"/gt";
$router->map( 'GET', $gameTestURI , function() {
	
		if(isset($_SESSION['gd']['uid'])){
		$users = new Users();
		$users->find($_SESSION['gd']['uid']);
		if($users->admin == 1){
			require __DIR__ . '/views/gameTest.php';
		} else {
			require __DIR__ . '/views/errors/404.php';
		}
	} else {
		require __DIR__ . '/views/errors/404.php';
	}
	
});

$toolTestURI = ($_SERVER['SERVER_NAME'] == "localhost")?"/streamminer/public/gt/[i:tool]":"/gt/[i:tool]";
$router->map( 'GET', $toolTestURI , function($tool) {
	require __DIR__ . '/views/gameTest.php';
});

$activateURI = ($_SERVER['SERVER_NAME'] == "localhost")?"/streamminer/public/activate":"/activate";
$router->map( 'GET', $activateURI , function() {
	$token = null;
	require __DIR__ . '/views/activate.php';
});

$router->map( 'POST', $activateURI , function() {
	require __DIR__ . '/../src/handlers/loginHandler.php';
});

$resendActivationURI = ($_SERVER['SERVER_NAME'] == "localhost")?"/streamminer/public/resendActivation":"/resendActivation";
$router->map( 'GET', $resendActivationURI , function() {
	$token = null;
	require __DIR__ . '/views/resendActivation.php';
});

$router->map( 'POST', $resendActivationURI , function() {
	require __DIR__ . '/../src/handlers/loginHandler.php';
});



$activateTokenURI = ($_SERVER['SERVER_NAME'] == "localhost")?"/streamminer/public/activate/[a:token]":"/activate/[a:token]";
$router->map( 'GET', $activateTokenURI , function($token) {
	require __DIR__ . '/views/activate.php';
});

$gameURI = ($_SERVER['SERVER_NAME'] == "localhost")?"/streamminer/public/game":"/game";
$router->map( 'GET', $gameURI , function() {
	require __DIR__ . '/views/game.php';
});

$gameOnlyURI = ($_SERVER['SERVER_NAME'] == "localhost")?"/streamminer/public/game-only":"/game-only";
$router->map( 'GET', $gameOnlyURI , function() {
	require __DIR__ . '/views/gameOnly.php';
});

// Special request mapping (Specific users, game shop / leaderboard pages)
$userURI = ($_SERVER['SERVER_NAME'] == "localhost")?"/streamminer/public/u/[:user]":"/u/[:user]";
$router->map( 'GET', $userURI, function($user) {
	require __DIR__ . '/views/specificUser.php';
});

$userPopoutURI = ($_SERVER['SERVER_NAME'] == "localhost")?"/streamminer/public/u/[:user]/popout":"/u/[:user]/popout";
$router->map( 'GET', $userPopoutURI, function($user) {
	require __DIR__ . '/views/popoutUser.php';
});

$userCheckURI = ($_SERVER['SERVER_NAME'] == "localhost")?"/streamminer/public/userCheck":"/userCheck";
$router->map( 'POST', $userCheckURI, function() {
	require __DIR__ . '/../src/handlers/siteHandler.php';
});

$leaderboardURI = ($_SERVER['SERVER_NAME'] == "localhost")?"/streamminer/public/leaderboard":"/leaderboard";
$router->map( 'GET', $leaderboardURI, 'leaderboardMapping');
$router->map( 'GET', $leaderboardURI."s", 'leaderboardMapping');
$router->map( 'GET', $leaderboardURI."/", 'leaderboardMapping');
$router->map( 'GET', $leaderboardURI."s/", 'leaderboardMapping');
function leaderboardMapping() {
	$lbActiveTab = "daily";
	$leaderboardPage = "streams";
	require __DIR__ . '/views/leaderboards.php';
}

$streamLeaderboardURI = ($_SERVER['SERVER_NAME'] == "localhost")?"/streamminer/public/leaderboards/streams":"/leaderboards/streams";
$router->map( 'GET', $streamLeaderboardURI, function() {
	$lbActiveTab = "daily";
	$leaderboardPage = "streams";
	require __DIR__ . '/views/leaderboards.php';
});

$streamLeaderboardTimeFrameURI = ($_SERVER['SERVER_NAME'] == "localhost")?"/streamminer/public/leaderboards/streams/[a:timeFrame]":"/leaderboards/streams/[a:timeFrame]";
$router->map( 'GET', $streamLeaderboardTimeFrameURI, "streamTimeFrame");
function streamTimeFrame($timeFrame) {
	$lbActiveTab = strtolower($timeFrame);
	$leaderboardPage = "streams";
	require __DIR__ . '/views/leaderboards.php';
}

$playerLeaderboardURI = ($_SERVER['SERVER_NAME'] == "localhost")?"/streamminer/public/leaderboards/players":"/leaderboards/players";
$router->map( 'GET', $playerLeaderboardURI, function() {
	$lbActiveTab = "daily";
	$leaderboardPage = "players";
	require __DIR__ . '/views/leaderboards.php';
});

$playerLeaderboardTimeFrameURI = ($_SERVER['SERVER_NAME'] == "localhost")?"/streamminer/public/leaderboards/players/[a:timeFrame]":"/leaderboards/players/[a:timeFrame]";
$router->map( 'GET', $playerLeaderboardTimeFrameURI, function($timeFrame) {
	$lbActiveTab = strtolower($timeFrame);
	$leaderboardPage = "players";
	require __DIR__ . '/views/leaderboards.php';
});

$getLeaderboardDataURI = ($_SERVER['SERVER_NAME'] == "localhost")?"/streamminer/public/leaderboardData":"/leaderboardData";
$router->map( 'GET', $getLeaderboardDataURI, function() {
	require __DIR__ . '/../src/handlers/getLeaderboardData.php';
});

/* 
$searchLeaderboardURI = ($_SERVER['SERVER_NAME'] == "localhost")?"/streamminer/public/leaderboards/[a:search]":"/leaderboards/[a:search]";
$router->map( 'GET', $searchLeaderboardURI, function($searchFor) {
	$leaderboardPage = "leaderboardSearch";
	require __DIR__ . '/views/leaderboards.php';
});
 */



$profileURI = ($_SERVER['SERVER_NAME'] == "localhost")?"/streamminer/public/profile/[a:user]":"/profile/[a:user]";
$router->map( 'GET', $profileURI, function($user) {
	if($user != ""){
		$smh = new SmHelper;
		$user = $smh->getProfileData($user);
		require __DIR__ . '/views/userProfile.php';
	} else {
		require __DIR__ . '/views/errors/404.php';
	}
});

$dashboardURI = ($_SERVER['SERVER_NAME'] == "localhost")?"/streamminer/public/dashboard":"/dashboard";
$router->map( 'GET', $dashboardURI, function() {
	require __DIR__ . '/views/dashboard.php';
});

$router->map( 'POST', $dashboardURI, function() {
	require __DIR__ . '/../src/handlers/siteHandler.php';
});

$adminURI = ($_SERVER['SERVER_NAME'] == "localhost")?"/streamminer/public/admin":"/admin";
$router->map( 'GET', $adminURI, function() {
	if(isset($_SESSION['gd']['uid'])){
		$users = new Users();
		$users->find($_SESSION['gd']['uid']);
		if($users->admin == 1){
			require __DIR__ . '/views/admin.php';
		} else {
			require __DIR__ . '/views/errors/404.php';
		}
	} else {
		require __DIR__ . '/views/errors/404.php';
	}
	
});


// Post handler mapping
$saveClicksURI = ($_SERVER['SERVER_NAME'] == "localhost")?"/streamminer/public/saveClicks":"/saveClicks";
$router->map( 'POST', $saveClicksURI , function() {
	require __DIR__ . '/../src/handlers/saveClicks.php';
});

$pageHandlerURI = ($_SERVER['SERVER_NAME'] == "localhost")?"/streamminer/public/pageFunctions":"/pageFunctions";
$router->map( 'POST', $pageHandlerURI , function() {
	require __DIR__ . '/../src/handlers/siteHandler.php';
});


$router->map( 'POST', $registerURI , function() {
	require __DIR__ . '/../src/handlers/loginHandler.php';
});

// match current request url
$match = $router->match();

// call closure or throw 404 status
if( $match && is_callable( $match['target'] ) ) {
	call_user_func_array( $match['target'], $match['params'] );
} else {
	// no route was matched
	header( $_SERVER["SERVER_PROTOCOL"] . ' 404 Not Found');
	require __DIR__ . '/views/errors/404.php';
}
?>