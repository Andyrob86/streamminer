var sidebarOpen = true;
var logoutAlertOpen = false;


function popoutGame(){
	if($("#gameDiv").length){
		gameHeight = $("#gameDiv").height();
		$("#gameDiv").remove();
		if($("#chat_embed").length){
			chatHeight = $("#chat_embed").height() + gameHeight;
			$("#chat_embed").height(chatHeight);
		}
		window.open(homeUrl + "game-only","","width=500,height=500,menubar=no,location=no,toolbar=no,status=no,titlebar=no");
	}
}

function purchaseItem(itemID) {
	
	swal({
		title: "Confirm",
		text: "Are you sure you want to purchase this item?<div class=\"well well-sm m-t-10\"><h3>" + $("#item_"+itemID+"_name").html() + "</h3>" + $("#item_"+itemID+"_image").html() + "<br/>Cost: " + $("#item_"+itemID+"_cost").html() + "</div>",
		html: true,
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#33CEA8",
		confirmButtonText: "Yes, gimme!",
		cancelButtonText: "No, let me think about it!",
		closeOnConfirm: false,
		closeOnCancel: false
	}, function(isConfirm){
		if (isConfirm) {
			$.ajax({
				url: homeUrl + "shop",
				type: "POST",
				data: {purchaseItem: true, id: itemID},
				dataType: "json"
			})
				.done(function(data){
					if(data.error){
						swal({
							title: "Error!",
							text: data.message,
							type: "error"
						});
					} else {
						swal({
							title: "Congrats!",
							text: data.message,
							type: "success"
						});
						var prevItem = itemID - 1;
						var nextItem = itemID + 1;
						$("#item_"+itemID+"_message_div").html("<div class=\"well\"><h4>Current Item</h4><p>This is your current item.</p></div>");
							$("#item_"+itemID+"_panel").removeClass("panel-info");
							$("#item_"+itemID+"_panel").addClass("panel-success");
						if(itemID > 0){
							$("#item_"+prevItem+"_panel").removeClass("panel-success");
							$("#item_"+prevItem+"_panel").addClass("panel-default");
							$("#item_"+prevItem+"_message_div").html("<div class=\"well\"><h4>Purchased</h4><p>You already own this item.</p></div>");
						};
						if(data.pointsRemaining >= $("#item_"+nextItem+"_cost").data("cost")){
							$("#item_"+nextItem+"_panel").removeClass("panel-danger");
							$("#item_"+nextItem+"_panel").addClass("panel-info");
							$("#item_"+nextItem+"_btn_div").removeClass("hidden");
							$("#item_"+nextItem+"_well_div").html("<div class=\"alert alert-success\"> Would you like to purchase this item? </div>");
						} else {
							$("#item_"+nextItem+"_well_div").html("<div class=\"alert alert-danger\"> You do not have enough points saved up to purchase this item! </div>");
						};

					}
				})
		} else {
			swal("Cancelled", "Phew! that was close..", "error");
		}
	});
}

function userCheck(){
	var userCheckAjax = setInterval(function() {
		$.ajax({
			url: homeUrl + "userCheck",
			type: "POST",
			data: { userCheck: true },
			dataType: "json"
		})
			.done(function( data ) {
				if(data.warnLogout && !logoutAlertOpen){
					logoutAlertOpen = true;
					$.toast({
						heading: 'Inactivity Notice',
						text: '<p class="m-b-10">You are about to be logged out due to inactivity.</p><hr class="enhancedHr m-b-10"><p>Close this window to stay logged in.</p>',
						position: 'top-right',
						loaderBg:'#ff6849',
						icon: 'warning',
						hideAfter: data.warnLogoutInfo.timeLeft * 1000,
						afterHidden: function(){ 
							$.ajax({
								url: homeUrl + "userCheck",
								type: "POST",
								data: { updateTimeout: true },
								dataType: "json"
							})
						}
					});
				};
				if(data.logout){
					window.location.href = homeUrl + "logout/timeout";
				};
			});
	}, 20000);
}

function popoutStream(){
	if($("#streamPlayer").length){
		stream = $("#streamPlayer").data("stream");
		window.open(homeUrl + "u/"+stream+"/popout", "", "width=960,height=540,menubar=no,location=no,toolbar=no,status=no,titlebar=no");
	}
}
if($("#leaderboardStreamTop100").length){
	var streamUpdater = setInterval(function(){
		checkTopStreams();
	}, 10000);
}


function checkTopStreams(){
	var datatable = $("#leaderboardStreamTop100").dataTable().api();
	var active = $("#streamPlayer").attr("data-stream");
	$.ajax({
		url: homeUrl + "pageFunctions",
		type: "POST",
		data: {updateStream: true, active: active},
		dataType: "json"
	})
		.done(function(data){
			if(data.streamSwitch){
				newStream(data.name);
			};
			// console.log(data.updatedLeaders)
			datatable.clear();
			datatable.rows.add(data.updatedLeaders);
			datatable.draw();
		});
}

function updateStream(streamer){
	$("#streamPlayer").attr("data-stream", streamer);
	var player = new Twitch.Player("streamPlayer", {channel: streamer});
	player.addEventListener(Twitch.Player.ENDED, () => { });
	$("#chat_embed").attr("src","https://www.twitch.tv/" + streamer + "/chat");
	
}

function newStream(streamer){
	if(typeof toastActive == 'undefined'){
		var toastActive = false;
	};
	if(toastActive == false){
		toastActive = true;
		$.toast({
			heading: 'Stream Switching!',
			text: '<p class="m-b-10">The leaderboards have changed! '+streamer+' is the new leader! Stream will be switching in <span id="timeBeforeSwitch"></span> seconds.</p><hr class="enhancedHr m-b-10"><p>You can continue watching '+ $("#streamPlayer").data("stream") + ' by visiting <a href="'+homeUrl+'/u/'+$("#streamPlayer").data("stream")+'">StreamMiner.tv/u/'+$("#streamPlayer").data("stream")+'</a>.</p>',
			position: 'top-left',
			loaderBg:'#ff6849',
			icon: 'warning',
			afterHidden: function () { updateStream(streamer); },
			hideAfter: 5000,
			preventDuplicates: true
		});
		countDown(5,"timeBeforeSwitch");
	};
	
}

function popoutChat(){
	
}

var leaderboardIds = ["#leaderboardStreamTop100","#leaderboardStreamDaily","#leaderboardStreamWeekly","#leaderboardStreamMonthly","#leaderboardStreamAllTime","#leaderboardPlayerDaily","#leaderboardPlayerWeekly","#leaderboardPlayerMonthly","#leaderboardPlayerAllTime"];
var topLeaderboardIds = ["#leaderboardStreamTop100"];
var streamLeaderboards = ["#leaderboardStreamTop100","#leaderboardStreamDaily","#leaderboardStreamWeekly","#leaderboardStreamMonthly","#leaderboardStreamAllTime"]


for(var i = 0; i < leaderboardIds.length; i++){
	if($(leaderboardIds[i]).length){
		leaderBoardCreate(leaderboardIds[i]);
	};
}

function leaderBoardCreate(leaderboard){
	var scrollHeight = "500px";
	if(topLeaderboardIds.indexOf(leaderboard) != -1){
		dataName = leaderboard.replace("#","");
		$(leaderboard).DataTable( {
			ajax: homeUrl + "leaderboardData",
			columns: [
				{ title: "Rank" },
				{ render: function(data, type, row, meta) { return '<a href="' + homeUrl + 'u/' + data + '">' + data + '</a>';} },
				{ title: "Points" }
			],
			ordering: false,
			scrollCollapse: true,
			paging: false,
			searching: false,
			responsive: true,
			info: false
		});
	} else {
		dataName = leaderboard.replace("#","");
		if(streamLeaderboards.indexOf(leaderboard) != -1){
			$(leaderboard).DataTable( {
				data: lb[dataName],
				columns: [
						{ width: "5%", title: "Rank" },
						{ width: "40%", render: function(data, type, row, meta) { return '<a href="' + homeUrl + 'u/' + data + '">' + data + '</a>';}},
						{ width: "55%", title: "Points" }
				],
				ordering: false,
				responsive: true,
				info: false
			});
		} else {
			$(leaderboard).DataTable( {
				data: lb[dataName],
				columns: [
						{ width: "5%", title: "Rank" },
						{ width: "40%" },
						{ width: "55%", title: "Points" }
				],
				ordering: false,
				responsive: true,
				info: false
			});
		}
	};
	if($(leaderboard).hasClass("top-leaderboard")){
		if($(leaderboard+'_wrapper').length){
			$(leaderboard+'_wrapper').slimScroll({
				height: scrollHeight
			});
		};
	}
};

function abbreviateNumber(value) {
    var newValue = value;
    if (value >= 1000) {
        var suffixes = ["", "k", "m", "b","t"];
        var suffixNum = Math.floor( (""+value).length/3 );
        var shortValue = '';
        for (var precision = 2; precision >= 1; precision--) {
            shortValue = parseFloat( (suffixNum != 0 ? (value / Math.pow(1000,suffixNum) ) : value).toPrecision(precision));
            var dotLessShortValue = (shortValue + '').replace(/[^a-zA-Z 0-9]+/g,'');
            if (dotLessShortValue.length <= 2) { break; }
        }
        if (shortValue % 1 != 0)  shortNum = shortValue.toFixed(1);
        newValue = shortValue+suffixes[suffixNum];
    }
    return newValue;
}

function Beautify(what,floats)//turns 9999999 into 9,999,999
{
	var str='';
	what=Math.round(what*100000)/100000;//get rid of weird rounding errors
	if (floats>0)
	{
		var floater=what-Math.floor(what);
		floater=Math.round(floater*100000)/100000;//get rid of weird rounding errors
		var floatPresent=floater?1:0;
		floater=(floater.toString()+'0000000').slice(2,2+floats);//yes this is hacky (but it works)
		str=Beautify(Math.floor(what))+(floatPresent?('.'+floater):'');
	}
	else
	{
		what=Math.floor(what);
		what=(what+'').split('').reverse();
		for (var i in what)
		{
			if (i%3==0 && i>0) str=','+str;
			str=what[i]+str;
		}
	}
	return str;
}

function count_ref(time,loc,message){
	if (typeof time === 'undefined'){time = 5};
	if (typeof message === 'undefined'){message ="Page will reload automatically in "}
	(function countdown(remaining) {
    if(remaining <= 0)
        if (typeof loc === 'undefined'){
					//loc = location.reload()
				} else {
					window.location.href = loc;
				};
    document.getElementById('countDown').innerHTML = "<div class='text-center'>"+message+remaining+" seconds.</div>";
    setTimeout(function(){ countdown(remaining - 1); }, 1000);
})(time);
}

function countDown(time, elemId, refreshUrl){
	if (typeof time === 'undefined')time = 5;
	if (typeof elemId === 'undefined')elemId = "countDownNum";
	var refresh = (typeof refreshUrl === 'undefined')?false:true;
	(function countdown(remaining) {
		if(remaining >=0) {
			document.getElementById(elemId).innerHTML = remaining;
			setTimeout(function(){ countdown(remaining - 1); }, 1000);
			if (remaining == 0 && refresh === true)window.location.href = refreshUrl;
		}
	})(time);
}

$(".leaderTab").click(function(){
	$("#lastCrumb").text($(this).data("crumbpage"));
})

$("#hide1027alert").click(function(){
	$.ajax({
		url: homeUrl + "pageFunctions",
		type: "POST",
		data: {hide1027alert: true},
		dataType: "json"
	})
})


$("#hide-menu-tab").click(function(){
	$("#nav-sidebar").toggle("slide", 300);
	if(sidebarOpen){
		$("#page-wrapper").animate({
			margin: "0"
		},300,function(){
			$("#hide-icon").removeClass("fa-chevron-left");
			$("#hide-icon").addClass("fa-chevron-right");
			sidebarOpen = false;
		});
	} else {
		$("#page-wrapper").animate({
			margin: "0 0 0 250px"
		},300,function(){
			$("#hide-icon").removeClass("fa-chevron-right");
			$("#hide-icon").addClass("fa-chevron-left");
			sidebarOpen = true;
		});
	}
	
})

$("#representForm").submit(function(e){
	var stream = $("#representTarget").val();
	swal({
		title: "Confirm",
		text: "Did you type the streamer's name correctly?<div class=\"well well-sm m-t-10\"><h3>" + stream + "</h3></div>",
		html: true,
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#33CEA8",
		confirmButtonText: "Yes, it is correct!",
		cancelButtonText: "No, let me fix it!",
		closeOnConfirm: false,
		closeOnCancel: false
	}, function(isConfirm){
		if (isConfirm) {
			$.ajax({
				url: homeUrl + "represent",
				type: "POST",
				data: {representStream: true, stream: stream},
				dataType: "json"
			})
				.done(function(data){
					if(data.error){
						swal("Error!", data.message, "error");
					} else {
						swal("You're all set!", "You are now representing " + stream + ". Get to mining!", "success");
						$("#representTarget").val("");
					}
				});
		} else {
			swal("Cancelled", "Phew! that was close..", "error");
		}
	});
	e.preventDefault();
	
})

$("#donateForm").submit(function(e){
	var stream = $("#donateTarget").val();
	var points = $("#donateAmt").val();
	swal({
		title: "Confirm",
		text: "Did you type the streamer's name correctly?<div class=\"well well-sm m-t-10\"><h3>" + stream + "</h3><p>Points to donate: " + points + "</p></div>",
		html: true,
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#33CEA8",
		confirmButtonText: "Yes, it is correct!",
		cancelButtonText: "No, let me fix it!",
		closeOnConfirm: false,
		closeOnCancel: false
	}, function(isConfirm){
		if (isConfirm) {
			$.ajax({
				url: homeUrl + "represent",
				type: "POST",
				data: {donateStream: true, stream: stream, points: points},
				dataType: "json"
			})
				.done(function(data){
					if(data.error){
						swal("Error!", data.message, "error");
					} else {
						swal("Donation sent!", "Your donation has been sent to " + stream + "'s stream!", "success");
						$("#donateTarget").val("");
						$("#donateAmt").val("");
					}
				});
		} else {
			swal("Cancelled", "Phew! that was close..", "error");
		}
	});
	e.preventDefault();
	
})

$("#loginform").submit(function(e){
	if($("#rememberMeCheck").is(':checked')){
		var rememberLogin = 1;
	} else {
		var rememberLogin = 0;

	}
	if($("#loginRedirect").length){
		redirectUrl = homeUrl + $("#loginRedirect").val();
	} else {
		redirectUrl = homeUrl;
	}
	$.ajax({
		url: homeUrl + "login",
		data: {login: true, email: $("#signInEmail").val(), pass: $("#signInPass").val(), remember: rememberLogin },
		type: "POST",
		dataType: "json"
	})
	.done(function(data){
		if(data.error){
			if(data.banned){
				swal({
					title: "Whoops!",
					text: data.message,
					imageUrl: homeUrl + "assets/img/banhamma.png",
					html: true
				});
			} else {
				swal({
					title: "Whoops!",
					text: data.message,
					type: "error"
				});
			};
		} else {
			swal({
				title: "Logged In!",
				text: "Going to Home page in 3 seconds, or just click OK.",
				type: "success",
				timer: 3000
			}, function(){
				window.location.href = redirectUrl;
			});
		}
	})
	e.preventDefault();
});

$(".userLogout").click(function(e){
	$.ajax({
		url: homeUrl + "logout",
		data: {logOut: true},
		type: "POST",
		dataType: "json"
	})
	.done(function(data){
		if(data.error){
			swal({
				title: "Hmmm!",
				text: data.message,
				type: "error"
			});
		} else {
			swal({
				title: "Logged Out!",
				text: "Returning to Home page in 3 seconds, or just click OK.",
				type: "success",
				timer: 3000
			}, function(){
				window.location.href = homeUrl + "login";
			});
		}
	})
	e.preventDefault();
})

$("#recoverform, #recoverpassform").submit(function(e){
	$.ajax({
		url: homeUrl + "login",
		data: {resetPass: true, email: $("#recoverEmail").val()},
		type: "POST",
		dataType: "json"
	})
	.done(function(data){
		if(data.error){
			swal({
				title: "Whoops!",
				text: data.message,
				type: "error"
			});
		} else {
			swal({
				title: "Password Reset Email Sent!",
				text: "Please check your inbox for a link to reset your password.",
				type: "success"
			});
		}
	})
	e.preventDefault();
});

$("#resetpassform").submit(function(e){
	$.ajax({
		url: homeUrl + "login",
		data: {updatePass: true, key: $("#resetKey").val(), pass: $("#newpass").val(), pass2: $("#newpass2").val()},
		type: "POST",
		dataType: "json"
	})
	.done(function(data){
		if(data.error){
			swal({
				title: "Whoops!",
				text: data.message,
				type: "error"
			});
		} else {
			swal({
				title: "Password Has Been Reset!",
				text: "You can now log in with your new password.",
				type: "success"
			});
		}
	})
	e.preventDefault();
});

$("#changePassForm").submit(function(e){
	$.ajax({
		url: homeUrl + "dashboard",
		data: {changePass: true, currentPass: $("#currentPass").val(), newpass: $("#newpass").val(), newpass2: $("#newpass2").val(),token:$("#token").val()},
		type: "POST",
		dataType: "json"
	})
	.done(function(data){
		if(data.error){
			swal({
				title: "Whoops!",
				text: data.message,
				type: "error"
			});
		} else {
			swal({
				title: "Password Has Been Reset!",
				text: "You will now need to login with your new password.",
				type: "success"
			}, function(){
				window.location.href = homeUrl + "logout";
			});
		}
	})
	e.preventDefault();
});

$("#registerForm").submit(function(e){
	$.ajax({
		url: homeUrl + "register",
		data: {register: true, email: $("#registerEmail").val(), pass: $("#registerPass").val(), pass2: $("#registerPass2").val(), userName: $("#registerUserName").val() },
		type: "POST",
		dataType: "json"
	})
	.done(function(data){
		if(data.error){
			swal({
				title: "Whoops!",
				text: data.message,
				type: "error"
			});
		} else {
			swal({
				title: "Thanks for signing up!",
				text: "You will need to activate your account before you can log in! Please check your email for your account activation link.",
				type: "success"
			}, function(){
				window.location.href = homeUrl + "activate";
			});
		}
		
	})
	e.preventDefault();
});

$("#activateForm").submit(function(e){
	$.ajax({
			url: homeUrl + "activate",
			data: {activateUser: true, key: $("#activationKey").val()},
			type: "POST",
			dataType: "json"
		})
		.done(function(data){
			if(data.error){
				swal({
					title: "Whoops!",
					text: data.message,
					type: "error"
				});
			} else {
				swal({
					title: "Account Activated!",
					text: "You can now log into your Stream Miner account.",
					type: "success"
				}, function(){
					window.location.href = homeUrl + "login";
				});
			}
		})
	e.preventDefault();
})

$("#resendKeyForm").submit(function(e){
	$.ajax({
			url: homeUrl + "resendActivation",
			data: {resendActivationKey: true, email: $("#resendKeyEmail").val()},
			type: "POST",
			dataType: "json"
		})
		.done(function(data){
			if(data.error){
				swal({
					title: "Whoops!",
					text: data.message,
					type: "error"
				});
			} else {
				swal({
					title: "Activation Key Resent!",
					text: "Check your email for your new activation key.",
					type: "success"
				}, function(){
					window.location.href = homeUrl + "activate";
				});
			}
		})
	e.preventDefault();
})

/*
function setChatHeight(){
	if($("#chat_embed").length && $("#streamPlayer").length){
		curChatHeight = $("#chat_embed").height();
		curStreamHeight = $("#streamPlayer").height();
		console.log("chatHeight: " + curChatHeight);
		console.log("streamHeight: " + curStreamHeight);
		if($("#gameDiv").length){
			curGameHeight = $("#gameDiv").height();
			chatHeight = curStreamHeight - curGameHeight;
		console.log("streamHeight: " + curStreamHeight);
		} else {
			chatHeight = curStreamHeight;
		}
		$("#chat_embed").height(chatHeight);
		console.log(chatHeight);
	}
};
*/

$(document).ready(function(){
	$(function(){
		 window.scrollTo(0, 0);
	});
	userCheck();
});


