var gameStarted = false;
var titleOut = false;
var mainTileSize = 500;
var rockClickSpriteSize = 30;
var toolSpriteSize = 200;
var rawClicks = 0;
var toolMulti = [1,1,2,4,6,8,10,13,15,17,20,23,25,28,31,34,37,40,43,46,49,52,56,59,62,66,69,73,76,80,83,87,91,94,100];
var scrollAmount = 600;
var counter = 0;
var gameMargin = 25;
if(typeof points == "undefined"){
	var points = 0;
};
if(typeof toolLevel == "undefined"){
	var toolLevel = 1;
	var toolNum = 0;
	var toolSheet = 0;
} else {
	var toolNum = ((toolLevel - 1) % 5)  * 3;
	var toolSheet = Math.floor(toolLevel / 5);
	if(toolLevel % 5 == 0) toolSheet--;
};
if(toolNum < 0)toolNum = 0;

function saveClicks(){
	$.ajax({
			method: "POST",
			url: homeUrl + "saveClicks",
			data: { data: 1, clicks: points }
		});
}

$(window).unload(function(){
  $.ajax({
			method: "POST",
			url: homeUrl + "saveClicks",
			async: false,
			data: { data: 1, clicks: points }
		});
});

$(window).bind('beforeunload', function(){
  $.ajax({
			method: "POST",
			url: homeUrl + "saveClicks",
			async: false,
			data: { data: 1, clicks: points, raw: rawClicks }
		});
});

window.addEventListener("beforeunload", function(e){
   saveClicks();
}, false);

function runUpdateClicks(){
	var updateClicks = setInterval(function() {
		$.ajax({
			method: "POST",
			url: homeUrl + "saveClicks",
			data: { data: 1, clicks: points, raw: rawClicks }
		})
			.done(function( msg ) {
				console.log( "Data Saved: " + msg );
				rawClicks = 0;
			});
	}, 10000);
}


var gameStart = function(){
	if(!gameStarted){
		function getRandom(min, max) {
			return Math.random() * (max - min) + min;
		};
		function getRandomInt(min, max) {
			return Math.floor(Math.random() * (max - min + 1) + min);
		};
		
		var tool;
		var toolSwingAnim;
		var worldGroup;
		var backgroundGroup;
		var middleGroup;
		var foregroundGroup;
		var pointsArray = [];
		var pointColors = ["0x00ff00", "0x008800", "0x880000", "0xff0000"];
		var game = new Phaser.Game(800,800, Phaser.AUTO, 'gameDiv',"PreloadAssets",true);
		var preloadAssets = function(game){}
		preloadAssets.prototype = {
			preload: function(){
				game.load.spritesheet("main", homeUrl + "assets/sprites/mainsprites.png", mainTileSize, mainTileSize);
				game.load.spritesheet("rockSmall", homeUrl +  "assets/sprites/smallsprites.png", rockClickSpriteSize, rockClickSpriteSize, 10 );
				game.load.spritesheet("tool", homeUrl +  "assets/sprites/toolSprites"+toolSheet+".png", toolSpriteSize, toolSpriteSize);
				game.load.image("bg", homeUrl + "assets/img/game/bg_test.jpg");
				game.load.image("textLogo", homeUrl + "assets/img/game/SMText.png");
			},
			create: function(){
				game.state.start("PlayGame");
			}
		}
		/*
		var titleScreen = function(game){}
		titleScreen.prototype = {
			create: function(){
				game.stage.backgroundColor = "#ffffff";
				game.scale.pageAlignHorizontally = true;
				game.scale.pageAlignVertically = true;
				game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
				game.stage.disableVisibilityChange = true;
				var titleStyle = {
					align: "center"
				};
				var subStyle = {
					font: "32px Monospace",
					align: "center"
				};
				var titleImage = game.add.image(game.width / 2, game.height / 2 - 180, "textLogo", titleStyle);
				titleImage.anchor.set(0.5);
				var text = game.add.text(game.width / 2, game.height / 2 - 100, "Click to Start", subStyle);
				text.anchor.set(0.5);
				game.input.onDown.add(this.startGame, this);
			},
			startGame: function(){
				game.state.start("PlayGame");
			}
		}
		*/
		var playGame = function(game){}
		playGame.prototype = {
			countText: null,
			create: function(){
				game.stage.backgroundColor = "#ffffff";
				game.scale.pageAlignHorizontally = true;
				game.scale.pageAlignVertically = true;
				game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
				game.stage.disableVisibilityChange = true;
				game.physics.startSystem(Phaser.Physics.ARCADE);
				worldGroup = game.add.group();
				backgroundGroup = game.add.group();
				middleGroup = game.add.group();
				foregroundGroup = game.add.group();
				titleStyle = {
					align: "center"
				};
				var subStyle = {
					font: "32px Monospace",
					align: "center",
					fill: "#ffffff"
				};
				game.world.setBounds(gameMargin, gameMargin, 800, 800);
				fields = game.add.sprite(-20, -20, 'bg');
				startingTitleImage = game.add.sprite(gameMargin + game.width / 2, gameMargin + game.height / 2 - 180, "textLogo", titleStyle);
				startingTitleImage.anchor.set(0.5);
				titleText = game.add.text(gameMargin + game.width / 2, gameMargin + game.height / 2 - 100, "Click to Start", subStyle);
				titleText.anchor.set(0.5);
				worldGroup.add(fields);
				worldGroup.add(startingTitleImage);
				worldGroup.add(titleText);
				startingTitleImage.inputEnabled = true;
        startingTitleImage.input.useHandCursor = true;
				startingTitleImage.events.onInputDown.add(this.startGame, this);
				
				
				
			},
			update: function(){
				if(typeof tool != 'undefined'){
					if (game.physics.arcade.distanceToPointer(tool, game.input.activePoint) > 18){
						game.physics.arcade.moveToPointer(tool,600);
					} else {
						tool.body.velocity.set(0);
					};
				};
			},
			titleClick: function(){
				
			},
			startGame: function(){
				startingTitleImage.destroy();
				titleText.destroy();
				titleImage = game.add.sprite(game.width / 2, game.height / 2 - 180, "textLogo", titleStyle);
				titleImage.anchor.set(0.5);
				tool = game.add.sprite(game.world.centerX, game.world.centerY, "tool", toolNum);
				tool.anchor.set(0.2,1);
				game.physics.arcade.enable(tool);
				tool.animations.add('swing', [toolNum, toolNum+1, toolNum+2, toolNum], 30, false);
				foregroundGroup.add(tool);
				var countStyle = {
					font: "42px Monospace",
					align: "center",
					fontWeight: "bold",
					fill: "#ffffff"
				};
				leftSpace = (game.width - mainTileSize)/2;
				topSpace = game.height - mainTileSize;
				rocks = game.add.button(gameMargin + leftSpace, gameMargin -700, "main", this.rockClick, this);
				this.countText = game.add.text(gameMargin + 5,gameMargin + 5,"Points: "+ points, countStyle);
				backgroundGroup.add(rocks);
				backgroundGroup.add(this.countText);
				game.physics.enable(titleImage, Phaser.Physics.ARCADE);
				var titleExit = game.add.tween(titleImage).to({
					y: -20,
					alpha:0
				}, 900, "Quart.easeOut");
				var rockEnter = game.add.tween(rocks).to({
					y:topSpace,
					
				}, 500);
				titleExit.onComplete.add(function(){
					runUpdateClicks();
					rockEnter.start();
				});
				rockEnter.onComplete.add(function(){
					game.camera.shake(0.05, 200);
				});
				titleExit.start();
				

			},
			rockClick: function(target){
				tool.animations.play('swing');
				var crackNum = getRandomInt(0,4);
				var chipNum = getRandomInt(5,9);
				var chipAng = getRandomInt(-360, 360);
				var chipX = [];
				var chipY = [];
				chipX[0] = game.input.x;
				chipY[0] = game.input.y;
				chipX[1] = chipX[0];
				chipY[1] = chipY[0] - getRandomInt(50,250);
				chipX[2] = chipX[1] + getRandomInt(-170,170);
				chipY[2] = getRandomInt(chipY[0],chipY[1]);
				chipX[3] = chipX[2];
				chipY[3] = chipY[0] + getRandomInt(0,200);
				
				
				/* Visual points for bezier curve */
				// for(var i = 0; i < 4; i++){
					// console.log("chipX" + i + ": " + chipX[i] + ", chipY" + i + ": " +  chipY[i]);
				// }
				// for(var i = 0; i < 4; i++){
					// if (pointsArray[i] != null){
						// pointsArray[i].destroy()
					// };
					 // var draggablePoint = game.add.sprite(chipX[i], chipY[i], "point");  
					 // draggablePoint.inputEnabled = true;
					 // draggablePoint.tint = pointColors[i];
					 // draggablePoint.input.enableDrag(); 
					 // draggablePoint.anchor.set(0.5);  
					 // pointsArray[i] = draggablePoint; 
				// }
				
				
				points = points + toolMulti[(toolLevel - 1)];
				rawClicks++;
				target.frame = 1;
				game.time.events.add(50, this.resetFrame, this, target);
				var crackFx = game.add.sprite(gameMargin + game.input.x, gameMargin + game.input.y, "rockSmall", crackNum);
				crackFx.anchor.setTo(0.5, 0.5);
				crackFx.height = 100;
				crackFx.width = 100;
				crackFx.alpha = 1;
				
				middleGroup.add(crackFx);
				
				if(toolLevel >= 2){
					var chipFx = game.add.sprite(gameMargin + game.input.x, gameMargin + game.input.y, "rockSmall", chipNum);
					chipFx.anchor.setTo(0.5,0.5);
					chipFx.alpha = 1;
					chipFx.width = 50;
					chipFx.height = 50;
					
					middleGroup.add(chipFx);
					var chipFxTween = game.add.tween(chipFx).to({
						x: [chipX[0], chipX[1], chipX[2], chipX[3]],
						y: [chipY[0], chipY[1], chipY[2], chipY[3]],
						angle: chipAng,
						alpha:0
					}, 700, Phaser.Easing.Quadratic.InOut, true).interpolation(function(v, k){
						return Phaser.Math.bezierInterpolation(v, k);
					}); 
					
					chipFxTween.onComplete.add(function(){
						chipFx.destroy();
					});
				};
				
				var crackFxTween = game.add.tween(crackFx).to({
					alpha: 0
				}, 150, Phaser.Easing.Linear.None, true, 0);
				
				crackFxTween.onComplete.add(function(){
					crackFx.destroy();
				});
								
				this.countText.text = "Points: "+ points;
				if(points%69 == 0){
					//$("#testJ").html("( ͡° ͜ʖ ͡°)");
				} else {
					//$("#testJ").html("");
				}
			},
			scrollRight: function(){
				game.camera.x+=2;
				counter+=2;
				console.log(counter);
			},
			resetFrame: function(target){
				target.frame = 0;
			},
			rockClickFx: function() {
				
			}
		}
		game.state.add("PreloadAssets", preloadAssets);
		game.state.add("PlayGame", playGame);
		// game.state.add("TitleScreen", titleScreen);
		gameStarted = true;
	}
}