<?php
include '../vendor/autoload.php';
include '../src/classes/twitch/twitch_interface_edited.php';

class SmHelper {
	var $clickLimit = 25;
	var $toolMulti = [1,1,2,4,6,8,10,13,15,17,20,23,25,28,31,34,37,40,43,46,49,52,56,59,62,66,69,73,76,80,83,87,91,94,100];
	var $streamDataCacheTime = (5*60); // 5 min
	var $leaderCheckLimit = 50;
	
	function getPoints($rawClicks, $clicks, $current, $tool, $boost = null){
		$tool-= 1; //offset for $toolMulti array
		$pointsRaw = $rawClicks * $this->toolMulti[$tool];
		if ( $clicks <= $current + $pointsRaw || $clicks >= $current - $pointsRaw ) { $points = $clicks; } else { $points = $current; };
		return $points;
	}
	
	function test(){
		echo "test";
		return;
	}
	
	function getDailyLeaderData(){
		$db = new DB();
		$leaders = $db->query("SELECT id, stream FROM leaderboard_stream_daily ORDER BY points DESC");
		$limit = 0;
		foreach($leaders as $key => $leader){
			if ($limit == $this->leaderCheckLimit){
				break;
			}
			$daily_leader['name'] = $leader['stream'];
			$daily_leader['id'] = $leader['id'];
			$streamData = $this->getStreamData($leader['stream']);
			$daily_leader['stream'] = $streamData;
			
			if ($streamData['stream'] != "Offline"){
				return $daily_leader;
			}
			$limit++;
		}
		$daily_leader['name'] = "BleedTheWay";
		$daily_leader['id'] = 1;
		$daily_leader['stream'] = $this->getStreamData('bleedtheway');
		return $daily_leader;
	}
	
	function getProfileData($user){
		$data['name'] = "User Not Found";
		$data['dailyPoints'] = "No Data";
		$data['allTimePoints'] = "No Data";
		$db = new DB();
		$db->bind("username",$user);
		if($results = $db->row("SELECT id, player_name FROM game_data WHERE player_name = :username")){
			$data['name'] = $results['player_name'];
			$db->bind("id",$results['id']);
			$data['dailyPoints'] = $db->single("SELECT points FROM leaderboard_player_daily WHERE id = :id");
			$db->bind("id",$results['id']);
			$data['allTimePoints'] = $db->single("SELECT points FROM leaderboard_player_all_time WHERE id = :id");
		}
		return $data;
	}
	
	function getDashboardData($user){
		$data['name'] = "User Not Found";
		$data['dailyPoints'] = "No Data";
		$data['allTimePoints'] = "No Data";
		$db = new DB();
		$db->bind("username",$user);
		if($results = $db->row("SELECT id, player_name, tool, joined, token FROM game_data WHERE player_name = :username")){
			$data['token'] = $results['token'];
			$data['name'] = $results['player_name'];
			$data['tool'] = $results['tool'];
			$data['joined'] = date('Y-m-d H:i:s',$results['joined']);
			$db->bind("id",$results['id']);
			$db->bind("tool",$results['tool']);
			$userData = $db->row("SELECT ud.id, ud.email, t.name as toolName, ld.points as ldPoints, lw.points as lwPoints, lm.points as lmPoints, at.points as atPoints FROM users ud JOIN tool_data t ON t.id = :tool JOIN leaderboard_player_daily ld ON ld.id = ud.id JOIN leaderboard_player_weekly lw ON lw.id = ud.id JOIN leaderboard_player_monthly lm ON lm.id = ud.id JOIN leaderboard_player_all_time at ON at.id = ud.id WHERE ud.id = :id");
			$data['toolName'] = $userData['toolName'];
			$data['dailyPoints'] = $userData['ldPoints'];
			$data['weeklyPoints'] = $userData['lwPoints'];
			$data['monthlyPoints'] = $userData['lmPoints'];
			$data['allTimePoints'] = $userData['atPoints'];
			$data['email'] = $userData['email'];
		}
		return $data;
	}
	
	function updateTopStreamData(){
		$data = json_decode(file_get_contents("../src/classes/streamminer/streamData.json"),true);
		$db = new DB();
		$interface = new twitch;
		$leaders = $db->query("SELECT * FROM leaderboard_stream_daily ORDER BY points DESC LIMIT 50");
		foreach($leaders as $leader){
			if(isset($data[$leader['id']])){
				if($data[$leader['id']]['lastUpdate'] <= time() - $this->streamDataCacheTime){
					$streamData = $interface->getStreamObject($leader['stream']);
					$status = ($streamData == null)?"Offline":"Online";
					$streamData = ($streamData == null)?"Offline":$streamData;
					$data[$leader['id']]['stream'] = $streamData;
					$data[$leader['id']]['lastUpdate'] = time();
					$ld = new leaderDaily(array("id"=>$leader['id'],"status"=>$status));
					$ld->Save();
				}
			} else {
				$streamData = $interface->getStreamObject($leader['stream']);
				$status = ($streamData == null)?"Offline":"Online";
				$streamData = ($streamData == null)?"Offline":$streamData;
				$data[$leader['id']]['stream'] = $streamData;
				$data[$leader['id']]['lastUpdate'] = time();
				$ld = new leaderDaily(array("id"=>$leader['id'],"status"=>$status));
				$ld->Save();
			}
			unset($streamData);
		};
		file_put_contents("../src/classes/streamminer/streamData.json", json_encode($data), LOCK_EX);
	}
	
	function getStreamData($streamer){
		$db = new DB();
		$streamID = $this->getStreamID($streamer);
		if(file_exists("../src/classes/streamminer/streamData/{$streamID}.json")){
			$data = json_decode(file_get_contents("../src/classes/streamminer/streamData/{$streamID}.json"),true);
			if($data['lastUpdate'] <= time() - $this->streamDataCacheTime){
				$interface = new twitch;
				$streamData = $interface->getStreamObject($streamer);
				$streamStatus = ($streamData == null)?"Offline":"Online";
				$ld = new leaderDaily(array("id"=>$streamID,"status"=>$streamStatus));
				$ld->Save();
				$data['stream'] = ($streamData == null)?"Offline":$streamData;
				$data['lastUpdate'] = time();
				file_put_contents("../src/classes/streamminer/streamData/{$streamID}.json", json_encode($data), LOCK_EX);
				return $data;
			} else {
				return $data;
			}
		} else {
			$interface = new twitch;
			$streamData = $interface->getStreamObject($streamer);
			$streamStatus = ($streamData == null)?"Offline":"Online";
			$ld = new leaderDaily(array("id"=>$streamID,"status"=>$streamStatus));
			$ld->Save();
			$data['stream'] = ($streamData == null)?"Offline":$streamData;
			$data['lastUpdate'] = time();
			file_put_contents("../src/classes/streamminer/streamData/{$streamID}.json", json_encode($data), LOCK_EX);
			return $data;
		}
	}
	
	function getStreamID ($streamer) {
		$db = new DB();
		$db->bind("stream",$streamer);
		if($id = $db->single("SELECT id FROM streams WHERE streamer = :stream")){
			return $id;
		} else {
			$db->bind("stream",$streamer);
			$db->query("INSERT INTO streams (streamer, source) VALUES (:stream, \"twitch\")");
			$id = $db->lastInsertId();
			return $id;
		}
	}
	
	function getLeaderboardPosition($leaderboard, $timeframe, $name){
		$db = new DB();
		$db->bind("name", $name);
		switch($leaderboard){
			case "stream":
				$leaderboard = "leaderboard_stream_{$timeframe}";
				$results = $db->row("SELECT x.id, x.position, x.stream, x.points FROM (SELECT t.id, t.stream, @rownum := @rownum + 1 AS position, t.points FROM $leaderboard t JOIN (SELECT @rownum := 0) r ORDER BY t.points DESC) x WHERE x.stream = :name");
				break;
			case "player":
				$leaderboard = "leaderboard_player_{$timeframe}";
				$results = $db->row("SELECT x.id, x.position, x.player, x.points FROM (SELECT t.id, t.player, @rownum := @rownum + 1 AS position, t.points FROM $leaderboard t JOIN (SELECT @rownum := 0) r ORDER BY t.points DESC) x WHERE x.player = :name");
				break;
		}
		return $results;
	}

	function setSessionData($uid){
		$db = new DB();
		$db->bind("uid",$uid);
		$results = $db->row("SELECT gd.id, gd.player_name, gd.points, gd.tool, gd.last_update, s.expiredate FROM game_data gd JOIN sessions s ON gd.id = s.uid WHERE gd.id = :uid");
		$gameSession = array();	
		$gameSession["uid"] = $uid;
		$gameSession['player'] = $results['player_name'];
		$gameSession['tool'] = $results['tool'];
		$gameSession['points'] = $results['points'];
		$gameSession['expire'] = strtotime($results['expiredate']);
		$gameSession['lastUpdate'] = time();
		if(isset($_SESSION['gd']['representing'])){
			$gameSession['representing'] = $_SESSION['gd']['representing'];
		};
		$_SESSION['gd']  = $gameSession;
	}

	function updateLoginExpire($uid){
		$time = date("Y-m-d H:i:s", time()+(30 * 60));
		$db = new DB();
		$db->bind("time", $time);
		$db->bind("uid", $uid);
		$db->query("UPDATE sessions SET expiredate = :time WHERE uid = :uid");
		$gd = $_SESSION['gd'];
		$gd['expire'] = time()+(30 * 60);
		$gd['lastUpdate'] = time()+(30 * 60);
		$_SESSION['gd'] = $gd;
	}

	function pageTitle($title = "Stream Miner", $user = null){
		if($user != null){
			echo "<div class=\"row bg-title m-b-0\"><div class=\"col-lg-3 col-md-4 col-sm-4 col-xs-12\"><h4 class=\"page-title\">";
		
			$rank = $this->getLeaderboardPosition("stream","daily",$user);
			if($rank == null){
				echo "Unranked!";
			} else {
				echo "Rank: {$rank['position']} | Points: {$rank['points']}";
			}
		} else {
			echo "<div class=\"row bg-title\"><div class=\"col-lg-3 col-md-4 col-sm-4 col-xs-12\"><h4 class=\"page-title\">";
			echo $title;
		};
		echo "</h5></div><div class=\"col-lg-9 col-sm-4 col-md-4 col-xs-12\"><ol class=\"breadcrumb\">";
		
		$crumbs = explode("/",$_SERVER["REQUEST_URI"]);
		$crumbs = array_filter($crumbs);
		$result = array(); $path = '';
		//might need to subtract one from the count...
		$count = count($crumbs);
		foreach($crumbs as $k=>$crumb){
			 $path .= '/' . $crumb;
			 $name = ucfirst(str_replace(array(".php","_"),array(""," "), $crumb));
			 if($k != $count){
					 echo "<li><a href=\"$path\">$name</a></li>";
			 } else {
					$currentPage = $name;
					 echo "<li class=\"active\"><span id=\"lastCrumb\">$name</span></li>";
			 }
		}
		echo"</ol></div></div>";
	}
		 
	function verifyClicks($lastPoints, $newPoints, $rawClicks, $tool, $boost = null, $lastUpdate){
		$elapsed = time() - $lastUpdate;
		if($raw <= ($elapsed * $clickLimit)) {
			return true;
		}
	}	 
		 
	function verifyRawClicks($rawClicks, $lastUpdate){
		if(is_numeric($lastUpdate)){
				$elapsed = time() - $lastUpdate;
			if($rawClicks <= ($elapsed * $this->clickLimit)) {
				return true;
			}
		}
	}
		 
	function leaderboardTick($player_id, $player, $points, $stream = null){
		$db = new DB();
		$time = time();
		$db->bindMore(array("id"=>$player_id,"player"=>$player,"points"=>$points,"time"=>$time,"newPoints"=>$points,"upTime"=>$time));
		$db->query("INSERT INTO leaderboard_player_daily (id, player, points, last_update) VALUES (:id, :player, :points, :time ) ON DUPLICATE KEY UPDATE points = points + :newPoints, last_update = :upTime");
		$tracking = new pointTracking(array("amt"=>$points,"from_id"=>$player_id,"to_id"=>$player_id,"time"=>$time,"type"=>"self_tick"));
		$tracking->Create();
		if($stream != null){
			$points = round($points * 0.20);
			$db->bind("stream",$stream);
			if($id = $db->single("SELECT id FROM streams WHERE streamer = :stream")){
				$db->bindMore(array("id"=>$id,"stream"=>$stream,"points"=>$points,"time"=>$time,"newPoints"=>$points,"upTime"=>$time));
				$db->query("INSERT INTO leaderboard_stream_daily (id, stream, points, last_update) VALUES (:id, :stream, :points, :time ) ON DUPLICATE KEY UPDATE points = points + :newPoints, last_update = :upTime");
				$tracking = new pointTracking(array("amt"=>$points,"from_id"=>$player_id,"to_id"=>$id,"time"=>$time,"type"=>"streamer_tick"));
				$tracking->Create();

			} else {
				$db->bind("stream",$stream);
				$db->query("INSERT INTO streams (streamer, source) VALUES (:stream, \"twitch\")");
				$id = $db->lastInsertId();
				$db->bindMore(array("id"=>$id,"stream"=>$stream,"points"=>$points,"time"=>$time,"newPoints"=>$points,"upTime"=>$time));
				$db->query("INSERT INTO leaderboard_stream_daily (id, stream, points, last_update) VALUES (:id, :stream, :points, :time ) ON DUPLICATE KEY UPDATE points = points + :newPoints, last_update = :upTime");
				$tracking = new pointTracking(array("amt"=>$points,"from_id"=>$player_id,"to_id"=>$id,"time"=>$time,"type"=>"streamer_tick"));
				$tracking->Create();
			
			}

		}
		return;
	}
	
	function donateToStream($stream, $playerID, $points){
		$db = new DB();
		$time = time();
		$gd = $db->row("SELECT gd.*, ld.points as ldPoints FROM game_data gd JOIN leaderboard_player_daily ld ON gd.id = :uid", array("uid"=>$playerID));
		if($points <= $gd['points'] && $points <= $gd['ldPoints']){
			$db->bind("stream",$stream);
			if($id = $db->single("SELECT id FROM streams WHERE streamer = :stream")){
				$db->bindMore(array("id"=>$id,"stream"=>$stream,"points"=>$points,"time"=>$time,"newPoints"=>$points,"upTime"=>$time));
				$db->query("INSERT INTO leaderboard_stream_daily (id, stream, points, last_update) VALUES (:id, :stream, :points, :time ) ON DUPLICATE KEY UPDATE points = points + :newPoints, last_update = :upTime");
				$tracking = new pointTracking(array("amt"=>$points,"from_id"=>$playerID,"to_id"=>$id,"time"=>$time,"type"=>"donation"));
				$tracking->Create();
				$pointsLeft = $gd['points'] - $points;
				$updatePlayer = new gameData(array("id"=>$playerID,"points"=>$pointsLeft));
				$updatePlayer->Save();
				$lbPointsLeft = $gd['ldPoints'] - $points;
				$updatePlayer = new leaderDailyPlayer(array("id"=>$playerID,"points"=>$lbPointsLeft,"last_update"=>time()));
				$updatePlayer->Save();
				return true;
			} else {
				$db->bind("stream",$stream);
				$db->query("INSERT INTO streams (streamer, source) VALUES (:stream, \"twitch\")");
				$id = $db->lastInsertId();
				$db->bindMore(array("id"=>$id,"stream"=>$stream,"points"=>$points,"time"=>$time,"newPoints"=>$points,"upTime"=>$time));
				$db->query("INSERT INTO leaderboard_stream_daily (id, stream, points, last_update) VALUES (:id, :stream, :points, :time ) ON DUPLICATE KEY UPDATE points = points + :newPoints, last_update = :upTime");
				$tracking = new pointTracking(array("amt"=>$points,"from_id"=>$playerID,"to_id"=>$id,"time"=>$time,"type"=>"donation"));
				$tracking->Create();
				$updatePlayer = new gameData(array("id"=>$playerID,"points"=>$pointsLeft));
				$updatePlayer->Save();
				$lbPointsLeft = $gd['ldPoints'] - $points;
				$updatePlayer = new leaderDailyPlayer(array("id"=>$playerID,"points"=>$lbPointsLeft,"last_update"=>time()));
				$updatePlayer->Save();
				return true;
			}
		}
		return;
	}

	function updateLeaderboards(){
		$db = new DB();
		$lb = $db->query("SELECT * FROM leaderboard_player_daily");
		for($i =0; $i < count($lb);$i++){
			$db->bindMore(array("id"=>$lb[$i]['id'],"player"=>$lb[$i]['player'],"points"=>$lb[$i]['points'],"time"=>time(),"newPoints"=>$lb[$i]['points'],"upTime"=>time()));
			$db->query("INSERT INTO leaderboard_player_weekly (id, player, points, last_update) VALUES (:id, :player, :points, :time) ON DUPLICATE KEY UPDATE points = points + :newPoints, last_update = :upTime");
			$db->bindMore(array("id"=>$lb[$i]['id'],"player"=>$lb[$i]['player'],"points"=>$lb[$i]['points'],"time"=>time(),"newPoints"=>$lb[$i]['points'],"upTime"=>time()));
			$db->query("INSERT INTO leaderboard_player_monthly (id, player, points, last_update) VALUES (:id, :player, :points, :time) ON DUPLICATE KEY UPDATE points = points + :newPoints, last_update = :upTime");
			$db->bindMore(array("id"=>$lb[$i]['id'],"player"=>$lb[$i]['player'],"points"=>$lb[$i]['points'],"time"=>time(),"newPoints"=>$lb[$i]['points'],"upTime"=>time()));
			$db->query("INSERT INTO leaderboard_player_all_time (id, player, points, last_update) VALUES (:id, :player, :points, :time) ON DUPLICATE KEY UPDATE points = points + :newPoints, last_update = :upTime");
		}
		$db->query("TRUNCATE leaderboard_player_daily");
		$lb = $db->query("SELECT * FROM leaderboard_stream_daily");
		for($i =0; $i < count($lb);$i++){
			$db->bindMore(array("id"=>$lb[$i]['id'],"stream"=>$lb[$i]['stream'],"points"=>$lb[$i]['points'],"time"=>time(),"newPoints"=>$lb[$i]['points'],"upTime"=>time()));
			$db->query("INSERT INTO leaderboard_stream_weekly (id, stream, points, last_update) VALUES (:id, :stream, :points, :time) ON DUPLICATE KEY UPDATE points = points + :newPoints, last_update = :upTime");
			$db->bindMore(array("id"=>$lb[$i]['id'],"stream"=>$lb[$i]['stream'],"points"=>$lb[$i]['points'],"time"=>time(),"newPoints"=>$lb[$i]['points'],"upTime"=>time()));
			$db->query("INSERT INTO leaderboard_stream_monthly (id, stream, points, last_update) VALUES (:id, :stream, :points, :time) ON DUPLICATE KEY UPDATE points = points + :newPoints, last_update = :upTime");
			$db->bindMore(array("id"=>$lb[$i]['id'],"stream"=>$lb[$i]['stream'],"points"=>$lb[$i]['points'],"time"=>time(),"newPoints"=>$lb[$i]['points'],"upTime"=>time()));
			$db->query("INSERT INTO leaderboard_stream_all_time (id, stream, points, last_update) VALUES (:id, :stream, :points, :time) ON DUPLICATE KEY UPDATE points = points + :newPoints, last_update = :upTime");
		}
		$db->query("TRUNCATE leaderboard_stream_daily");
	}
		 
	

}
?>