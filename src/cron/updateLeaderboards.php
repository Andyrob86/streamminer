<?php
include '/home/arobvps/streamminer.tv/src/classes/db/Db.class.php';
$db = new DB();
$lb = $db->query("SELECT * FROM leaderboard_player_daily");
for($i =0; $i < count($lb);$i++){
	$db->bindMore(array("id"=>$lb[$i]['id'],"player"=>$lb[$i]['player'],"points"=>$lb[$i]['points'],"time"=>time(),"newPoints"=>$lb[$i]['points'],"upTime"=>time()));
	$db->query("INSERT INTO leaderboard_player_weekly (id, player, points, last_update) VALUES (:id, :player, :points, :time) ON DUPLICATE KEY UPDATE points = points + :newPoints, last_update = :upTime");
	$db->bindMore(array("id"=>$lb[$i]['id'],"player"=>$lb[$i]['player'],"points"=>$lb[$i]['points'],"time"=>time(),"newPoints"=>$lb[$i]['points'],"upTime"=>time()));
	$db->query("INSERT INTO leaderboard_player_monthly (id, player, points, last_update) VALUES (:id, :player, :points, :time) ON DUPLICATE KEY UPDATE points = points + :newPoints, last_update = :upTime");
	$db->bindMore(array("id"=>$lb[$i]['id'],"player"=>$lb[$i]['player'],"points"=>$lb[$i]['points'],"time"=>time(),"newPoints"=>$lb[$i]['points'],"upTime"=>time()));
	$db->query("INSERT INTO leaderboard_player_all_time (id, player, points, last_update) VALUES (:id, :player, :points, :time) ON DUPLICATE KEY UPDATE points = points + :newPoints, last_update = :upTime");
}
$db->query("TRUNCATE leaderboard_player_daily");
$lb = $db->query("SELECT * FROM leaderboard_stream_daily");
for($i =0; $i < count($lb);$i++){
	$db->bindMore(array("id"=>$lb[$i]['id'],"stream"=>$lb[$i]['stream'],"points"=>$lb[$i]['points'],"time"=>time(),"newPoints"=>$lb[$i]['points'],"upTime"=>time()));
	$db->query("INSERT INTO leaderboard_stream_weekly (id, stream, points, last_update) VALUES (:id, :stream, :points, :time) ON DUPLICATE KEY UPDATE points = points + :newPoints, last_update = :upTime");
	$db->bindMore(array("id"=>$lb[$i]['id'],"stream"=>$lb[$i]['stream'],"points"=>$lb[$i]['points'],"time"=>time(),"newPoints"=>$lb[$i]['points'],"upTime"=>time()));
	$db->query("INSERT INTO leaderboard_stream_monthly (id, stream, points, last_update) VALUES (:id, :stream, :points, :time) ON DUPLICATE KEY UPDATE points = points + :newPoints, last_update = :upTime");
	$db->bindMore(array("id"=>$lb[$i]['id'],"stream"=>$lb[$i]['stream'],"points"=>$lb[$i]['points'],"time"=>time(),"newPoints"=>$lb[$i]['points'],"upTime"=>time()));
	$db->query("INSERT INTO leaderboard_stream_all_time (id, stream, points, last_update) VALUES (:id, :stream, :points, :time) ON DUPLICATE KEY UPDATE points = points + :newPoints, last_update = :upTime");
}
$db->query("TRUNCATE leaderboard_stream_daily");
?>