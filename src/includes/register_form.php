<?php 
$formLead = (isset($formLead))?"<h5 class=\"text-center\">$formLead</h5>":"";
$panelClass = (isset($panelClass))?"$panelClass ":"";
?>
<div class="login-panel panel panel-default <?php echo $panelClass; ?>">
	<div class="panel-heading">
		<h2 class="panel-title">Register</h2>
	</div>
	<div class="panel-body">
		<?php echo $formLead; ?>
		<div id="registerResponse"></div>
		<form role="form" method="POST" id="registerForm">
			<fieldset>
				<div class="form-group">
					<input class="form-control" placeholder="E-mail" id="registerEmail" name="email" type="email" autofocus>
				</div>
				<div class="form-group">
					<input class="form-control" placeholder="Password" id="registerPass" name="password" type="password" value="">
				</div>
				<div class="form-group">
					<input class="form-control" placeholder="Confirm Password" id="registerPass2" name="passwordConfirm" type="password" value="">
				</div>
				<div class="form-group">
					<label for="userName">Public Username (for leaderboards).</label>
					<input class="form-control" placeholder="Username" id="userName" name="userName" type="text" maxlength="20" value="">
				</div>
				<!-- Change this to a button or input when using this as a form -->
				<button type="submit" class="btn btn-lg btn-success btn-block">Register Account</button>
			</fieldset>
		</form>
	</div>
</div>
<?php unset($formLead); unset($panelClass); ?>