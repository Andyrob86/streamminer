<?php
$json = array();
$leaders = array();
$leaderboardEnabled = true;
if(!isset($db)){
	$db = new DB();
};
$leadersMonthly = $db->query("SELECT * FROM leaderboard_player_monthly ORDER BY points DESC");
for($i = 0; $i < count($leadersMonthly); $i++){
	$r = $i + 1;
	$leaders[] = [$r, $leadersMonthly[$i]['player'], $leadersMonthly[$i]['points']];
};
$json = json_encode($leaders);
?>
<div class="white-box">
<h3 class="box-title m-b-0">Montly Player Leaderboard</h3>
<p class="text-muted m-b-30">Updates daily at 05:00 AM CST - Resets last day of each month at 05:00 AM CST</p>
<div class="table-responsive">
<table id="leaderboardPlayerMonthly" class="table table-striped table-bordered table-hover leaderboard" cellspacing="0" width="100%">
	<thead>
		<th>Rank</th>
		<th>Player</th>
		<th>Points</th>
	</thead>
	<tfoot>
		<th>Rank</th>
		<th>Player</th>
		<th>Points</th>
	</tfoot>
</table>
</div>
</div>
<script type="text/javascript">
	if(typeof lb === 'undefined'){
		var lb = {"leaderboardPlayerMonthly": <?php echo($json); ?>};
	} else {
		lb["leaderboardPlayerMonthly"] = <?php echo($json); ?>;
	};
</script>