<?php
$json = array();
$leaders = array();
$leaderboardEnabled = true;
if(!isset($db)){
	$db = new DB();
};
$leadersAllTime = $db->query("SELECT * FROM leaderboard_stream_all_time ORDER BY points DESC");
for($i = 0; $i < count($leadersAllTime); $i++){
	$r = $i + 1;
	$leaders[] = [$r, $leadersAllTime[$i]['stream'], $leadersAllTime[$i]['points']];
};
$json = json_encode($leaders);
?>
<div class="white-box">
<h3 class="box-title m-b-0">All Time Stream Leaderboard</h3>
<p class="text-muted m-b-30">Updates daily at 05:00 AM CST</p>
<div class="table-responsive">
<table id="leaderboardStreamAllTime" class="table table-striped table-bordered table-hover leaderboard" cellspacing="0" width="100%">
	<thead>
		<th>Rank</th>
		<th>Stream</th>
		<th>Points</th>
	</thead>
	<tfoot>
		<th>Rank</th>
		<th>Stream</th>
		<th>Points</th>
	</tfoot>
</table>
</div>
</div>
<script type="text/javascript">
	if(typeof lb === 'undefined'){
		var lb = {"leaderboardStreamAllTime": <?php echo($json); ?>};
	} else {
		lb["leaderboardStreamAllTime"] = <?php echo($json); ?>;
	};
</script>