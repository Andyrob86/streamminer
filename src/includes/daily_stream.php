<?php
if(!isset($daily_leader)){
	$daily_leader = $smh->getDailyLeaderData();
};
?>
<div class="playerHeight">
    <div class="embed-responsive embed-responsive-16by9" id="streamPlayer" data-stream="<?php echo $daily_leader['name']; ?>">
        <?php if(!isset($twitchEmbedScript)) { ?><script id="twitchEmbedJS" src= "https://player.twitch.tv/js/embed/v1.js"></script><?php }; ?>
        <script type="text/javascript">
            var options = {
                    channel: "<?php echo $daily_leader['name']; ?>", 
                    //video: "{VIDEO_ID}"       
            };
            var player = new Twitch.Player("streamPlayer", options);
            player.setVolume(0.5);
            //player.addEventListener(Twitch.Player.READY, () => {  });
        </script>
    </div>
 </div>
<?php $twitchEmbedScript = true; ?>