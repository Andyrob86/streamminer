<?php
require_once '../vendor/autoload.php';
$dbhConf = parse_ini_file("../settings.ini.php");
$dbh = new PDO("mysql:host={$dbhConf['host']};dbname={$dbhConf['dbname']}", $dbhConf['user'], $dbhConf['password']);

$config = new PHPAuth\Config($dbh);
$auth   = new PHPAuth\Auth($dbh, $config);
?>