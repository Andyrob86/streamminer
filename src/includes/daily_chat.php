<?php
if(!isset($daily_leader)){
	$daily_leader = $smh->getDailyLeaderData();};
?>

<div id="chatDiv">
	<iframe
		frameborder="0" 
		scrolling="no" 
		id="chat_embed" 
		src="https://www.twitch.tv/<?php echo $daily_leader['name']; ?>/chat" 
		height="450" 
		width="100%">
	</iframe>
</div>