<?php 
$formLead = (isset($formLead))?"<h5 class=\"text-center\">$formLead</h5>":"";
$panelClass = (isset($panelClass))?"$panelClass ":"";
?>
<div class="login-panel panel panel-default <?php echo $panelClass; ?>">
	<div class="panel-heading">
		<h2 class="panel-title">Sign In</h2>
	</div>
	<div class="panel-body">
		<?php echo $formLead; ?>
		<div id="signInResponse"></div>
		<form role="form" method="POST" id="signInForm">
			<fieldset>
				<div class="form-group">
					<input class="form-control" placeholder="E-mail" id="signInEmail" name="email" type="email" autofocus>
				</div>
				<div class="form-group">
						<input class="form-control" placeholder="Password" id="signInPass" name="password" type="password" value="">
				</div>
				<div class="checkbox">
					<label>
						<input id="signInRemember" name="remember" type="checkbox" value="Remember Me">Remember Me
					</label>
				</div>
				<!-- Change this to a button or input when using this as a form -->
				<button type="submit" class="btn btn-lg btn-success btn-block">Login</button>
			</fieldset>
		</form>
	</div>
</div>
<?php unset($formLead); unset($panelClass); ?>