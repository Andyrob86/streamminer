<?php
include '../vendor/autoload.php';
include '../src/includes/enableAuth.php';

if(isset($_POST['login']) && $_POST['login']){
	$email = isset($_POST['email'])?$_POST['email']:"";
	$pass = isset($_POST['pass'])?$_POST['pass']:"";
	$remember = (isset($_POST['remember']))?$_POST['remember']:0;
	$response = $auth->login($email, $pass, $remember);
	if($response['error']){
		echo json_encode($response);
		return;
	} else {
		$gameSession = array();
		$hash = $response['hash'];
		
		$gi = new gameData();
		
		$id = $gameSession["uid"] = $auth->getUID($email);
		$gi->find($id);
		if ($gi->banned == 1){
			$response['error'] = true;
			$response['banned'] = true;
			$response['message'] = "You got a new tool! The Ban Hammer!. If you believe this ban is in error, please contact <a href=\"mailto:support@streaminer.tv\" >support@streamminer.tv</a>";
			echo json_encode($response);
			return;
		} else {
			$gameSession['player'] = $gi->player_name;
			$gameSession['tool'] = $gi->tool;
			$gameSession['points'] = $gi->points;
			$gameSession['expire'] = ($response['expire'] == 0)?time()+(30 * 60):$response['expire'];
			$gameSession['lastUpdate'] = time();
			$_SESSION['gd']  = $gameSession;
			setcookie('authID',$hash , $response['expire'], "/");
			echo json_encode($response);
			return;
		}
		
	}
}

if(isset($_POST['resetPass']) && $_POST['resetPass'] && isset($_POST['email'])){
	$email = $_POST['email'];
	echo json_encode($auth->requestReset($email));
	return;
}

if(isset($_POST['updatePass']) && $_POST['updatePass'] && isset($_POST['key']) && isset($_POST['pass']) && isset($_POST['pass2'])){
	$key = $_POST['key'];
	$pass = $_POST['pass'];
	$pass2 = $_POST['pass2'];
	echo json_encode($auth->resetPass($key,$pass,$pass2));
	return;
}


if(isset($_POST['register']) && $_POST['register']){
	$email = isset($_POST['email'])?$_POST['email']:"";
	$pass = isset($_POST['pass'])?$_POST['pass']:"";
	$pass2 = isset($_POST['pass2'])?$_POST['pass2']:"";
	$userName = isset($_POST['userName'])?$_POST['userName']:"bleedtheway";
	$db = new DB();
	$db->bind("uname",$userName);
	if ($db->query("SELECT player_name FROM game_data WHERE player_name = :uname")){
		$response['error'] = true;
		$response['message'] = "Username taken.";
		echo json_encode($response);
		return;
	} else {
		
		$response = $auth->register($email, $pass, $pass2, array(),null,true);
		if(!$response['error']){
			$factory = new RandomLib\Factory;
			$generator = $factory->getMediumStrengthGenerator();
			$token = $generator->generateString(32, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
			$uid = $auth->getUID($email);
			$gd = new gameData();
			$gd->id = $uid;
			$gd->player_name = $userName;
			$gd->last_update = time();
			$gd->joined = time();
			$gd->token = $token;
			if($gd->Create() == 1){
				echo json_encode($response);
				return;
			} else {
				$response['message'].= " But there was a problem setting up your game settings! Please contact us to sort this out!";
				echo json_encode($response);
				return;
			};
		} else {
			echo json_encode($response);
			return;
		}
	}
}

if(isset($_POST['activateUser']) && $_POST['activateUser'] && isset($_POST['key'])){
	$key = $_POST['key'];
	$response = $auth->activate($key);
	echo json_encode($response);
	return;
}

if(isset($_POST['resendActivationKey']) && $_POST['resendActivationKey'] && isset($_POST['email'])){
	$email = $_POST['email'];
	$response = $auth->resendActivation($email, true);
	echo json_encode($response);
	return;
}

if(isset($_POST['logOut']) && $_POST['logOut']){
	require_once __DIR__ . '/../../src/includes/enableAuth.php';
	session_unset();
	if($auth->logout($_COOKIE['authID'])){
		$response['error'] = false;
		$response['message'] = "You have logged out";	
	} else {
		$response['error'] = true;
		$response['message'] = "Something went wrong! If the problem continues, try visiting /logout.";		
	}
	echo json_encode($response);
	return;
	
}


if($_SERVER['SERVER_NAME'] == "localhost"){
	header("Location: /streamminer/public/");
} else {
	header("Location: /");
}

?>