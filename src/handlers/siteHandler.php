<?php
include '../vendor/autoload.php';
include '../src/includes/enableAuth.php';

if(isset($_POST['userCheck']) && $_POST['userCheck']){
	$data['warnLogout'] = false;
	$data['newMessage'] = false;
	$data['error'] = false;
	if(isset($_SESSION['gd'])){
		$gd = $_SESSION['gd'];
		if ( ( $gd['expire'] - time() ) <= 120 ){
			$data['warnLogout'] = true;
			$data['warnLogoutInfo']['timeLeft'] = $gd['expire'] - time();
		};
		if ( ( $gd['expire'] - time() ) <= 1 ){
			$data['logout'] = true;
		};
	}
	echo json_encode($data);
	return;
}

if(isset($_POST['updateTimeout']) && $_POST['updateTimeout']){
	if(isset($_SESSION['gd'])){
		$gd = $_SESSION['gd'];
		$gd['expire'] = time() + strtotime("+30 minutes");
		$_SESSION['gd'] = $gd;
	};
	return;
}

if(isset($_POST['hide1027alert']) && $_POST['hide1027alert']){
	setcookie("A_102716",1,time()+strtotime("+3 months"));
	return;
}

if(isset($_POST['updateStream']) && $_POST['updateStream']){
	$smh = new SmHelper();
	$db = new DB();
	$data['streamSwitch'] = false;
	$current = $_POST['active'];
	$top = $smh->getDailyLeaderData();
	if($top['name'] != $current){
		$data = $top;
		$data['streamSwitch'] = true;
	};
	$leadersDaily = $db->query("SELECT * FROM leaderboard_stream_daily ORDER BY points DESC limit 100");
	for($i = 0; $i < count($leadersDaily); $i++){
		$r = $i + 1;
		$leaders[] = [$r, $leadersDaily[$i]['stream'], $leadersDaily[$i]['points']];
	};
	$data['updatedLeaders'] =  $leaders;
	echo json_encode($data);
	return;
}

if(isset($_POST['purchaseItem']) && $_POST['purchaseItem']){
	if (isset($_POST['id'])){
		$id = $_POST['id'];
		if(isset($_SESSION['gd'])){
			$sd = $_SESSION['gd'];
			$uid = $sd['uid'];
			$td = new toolData();
			$gd = new gameData();
			$td->find($id);
			$gd->find($uid);
			if($td->id != null && $gd->id != null){
				if($td->cost <= $gd->points && $td->id == $gd->tool + 1){
					$newGd = new gameData();
					$newPoints = $newGd->points = $gd->points - $td->cost;
					$newGd->tool = $id;
					$newGd->id = $uid;
					$saved = $newGd->Save();
					if($saved > 0){
						$data['error'] = false;
						$data['message'] = "You have just purchased the {$td->name}! You now have {$newPoints} points.";
						$data['pointsRemaining'] = $newPoints;
						$data['newTool'] = $sd['tool'] = $id;
						$sd['points'] = $newPoints;
						$_SESSION['gd'] = $sd;
						echo json_encode($data);
						return;
					}
				}
			}
		}
	}
	$data['error'] = true;
	$data['message'] = "Something went wrong! Please try again. If this keeps up, please contact us!";
	echo json_encode($data);
	return;
}

if(isset($_POST['representStream']) && $_POST['representStream']){
	$error = "";
	if (isset($_POST['stream'])){
		$stream = $_POST['stream'];
		if(isset($_SESSION['gd'])){
			$_SESSION['gd']['representing'] = $stream;
			$data['error'] = false;
			echo json_encode($data);
			return;
		} else {
			$data['error'] = true;
			$data['message'] = "You must be logged in to represent a stream!". $error;
			echo json_encode($data);
			return;
		}
	}
	$data['error'] = true;
	$data['message'] = "Something went wrong! Please try again. If this keeps up, please contact us!". $error;
	echo json_encode($data);
	return;
}

if(isset($_POST['donateStream']) && $_POST['donateStream']){
	$error = "";
	if (isset($_POST['stream']) && isset($_POST['points'])){
		$stream = $_POST['stream'];
		$points = $_POST['points'];
		if(isset($_SESSION['gd'])){
			$smh = new SmHelper();
			if($smh->donateToStream($stream,$_SESSION['gd']['uid'],$points)){
				$data['error'] = false;
				echo json_encode($data);
				return;
			}
		} else {
			$data['error'] = true;
			$data['message'] = "You must be logged in to represent a stream!". $error;
			echo json_encode($data);
			return;
		}
	}
	$data['error'] = true;
	$data['message'] = "Something went wrong! Please try again. If this keeps up, please contact us!". $error;
	echo json_encode($data);
	
	return;
}

if(isset($_POST['changePass']) && $_POST['changePass'] && isset($_POST['token']) && isset($_POST['newpass']) && isset($_POST['newpass2'])){
	$id = (isset($_SESSION['gd']['uid']))?$_SESSION['gd']['uid']:"";
	$gd = new gameData();
	$gd->id = $id;
	$gd->find();
	if ($gd->token = $_POST['token']){
		$currentPass = $_POST['currentPass'];
		$pass = $_POST['newpass'];
		$pass2 = $_POST['newpass2'];
		echo json_encode($auth->changePassword($id,$currentPass,$pass,$pass2));
	} else {
		$data['error'] = true;
		$data['message'] = "Something went wrong!";
		echo json_encode($data);
	}
	return;
}



if($_SERVER['SERVER_NAME'] == "localhost"){
	header("Location: /streamminer/public/");
} else {
	header("Location: /");
}

?>