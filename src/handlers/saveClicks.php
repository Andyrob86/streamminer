<?php
require_once '../vendor/autoload.php';

if(isset($_POST['data']) && $_POST['data'] == 1 && isset($_POST['clicks']) && isset($_POST['raw'])){
	if(isset($_SESSION['gd'])){
		$smh = new SmHelper();
		$data = $_SESSION['gd'];
		$uid = intval($_SESSION['gd']['uid']);
		$clicks = $_POST['clicks'];
		$raw = $_POST['raw'];
		if($raw > 0){
			$smh->updateLoginExpire($uid);
			if($smh->verifyRawClicks($raw,$data['lastUpdate'])){
				$gd = new gameData();
				$db = new DB();
				$db->bind("id",$uid);
				$curPoints = $db->single("SELECT points FROM game_data WHERE id = :id");
				$newPoints = $smh->getPoints($raw, $clicks, $curPoints, $data['tool']);
				$data['points'] = $newPoints;
				if(isset($_SESSION['gd']['uid'])){
					$stream = (isset($data['representing']))?$data['representing']:null;
					$gd->points = $newPoints;
					$gd->Id = $uid;
					$gd->Save();
					$smh->leaderboardTick($uid,$data['player'],$newPoints,$stream);
				}
				$_SESSION['gd'] = $data;
			}
			$_SESSION['gd']['lastUpdate'] = time();
		}
		
		
	};
	return;
}


?>