CREATE DATABASE  IF NOT EXISTS `stream_miner` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `stream_miner`;
-- MySQL dump 10.13  Distrib 5.6.24, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: stream_miner
-- ------------------------------------------------------
-- Server version	5.6.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `attempts`
--

DROP TABLE IF EXISTS `attempts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attempts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(39) NOT NULL,
  `expiredate` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attempts`
--

LOCK TABLES `attempts` WRITE;
/*!40000 ALTER TABLE `attempts` DISABLE KEYS */;
/*!40000 ALTER TABLE `attempts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cheater_tracking`
--

DROP TABLE IF EXISTS `cheater_tracking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cheater_tracking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `extra` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cheater_tracking`
--

LOCK TABLES `cheater_tracking` WRITE;
/*!40000 ALTER TABLE `cheater_tracking` DISABLE KEYS */;
/*!40000 ALTER TABLE `cheater_tracking` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `config`
--

DROP TABLE IF EXISTS `config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config` (
  `setting` varchar(100) NOT NULL,
  `value` varchar(100) DEFAULT NULL,
  UNIQUE KEY `setting` (`setting`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `config`
--

LOCK TABLES `config` WRITE;
/*!40000 ALTER TABLE `config` DISABLE KEYS */;
INSERT INTO `config` VALUES ('attack_mitigation_time','+30 minutes'),('attempts_before_ban','10'),('attempts_before_verify','5'),('bcrypt_cost','10'),('cookie_domain',NULL),('cookie_forget','+30 minutes'),('cookie_http','0'),('cookie_name','authID'),('cookie_path','/'),('cookie_remember','+3 months'),('cookie_secure','0'),('emailmessage_suppress_activation','0'),('emailmessage_suppress_reset','0'),('mail_charset','UTF-8'),('password_min_score','2'),('request_key_expiration','+10 minutes'),('site_activation_page','activate'),('site_email','no-reply@miner.bleedtheway.com'),('site_key','Qirn659[AEHbcghjopvzROIq4,LMNSWdmuwy0'),('site_name','miner.bleedtheway.com'),('site_password_reset_page','reset'),('site_timezone','America/Chicago'),('site_url','http://miner.bleedtheway.com'),('smtp','0'),('smtp_auth','1'),('smtp_host','mail.bleedtheway.com'),('smtp_password','GYjlFAi80&LNPVdkmnrs'),('smtp_port','465'),('smtp_security','ssl'),('smtp_username','no-reply@miner.bleedtheway.com'),('table_attempts','attempts'),('table_requests','requests'),('table_sessions','sessions'),('table_users','users'),('verify_email_max_length','100'),('verify_email_min_length','5'),('verify_email_use_banlist','1'),('verify_password_min_length','6');
/*!40000 ALTER TABLE `config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `donation_tracking`
--

DROP TABLE IF EXISTS `donation_tracking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `donation_tracking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amt` int(11) DEFAULT NULL,
  `from_id` int(11) DEFAULT NULL,
  `to_id` int(11) DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `donation_tracking`
--

LOCK TABLES `donation_tracking` WRITE;
/*!40000 ALTER TABLE `donation_tracking` DISABLE KEYS */;
INSERT INTO `donation_tracking` VALUES (1,4200,1,1,1474845142,'self_tick'),(2,1120,1,1,1474845152,'self_tick'),(3,4480,1,1,1474845162,'self_tick'),(4,400,NULL,1,1474851321,'donation'),(5,400,1,1,1474851366,'donation'),(6,400,1,1,1474851408,'donation');
/*!40000 ALTER TABLE `donation_tracking` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `donations`
--

DROP TABLE IF EXISTS `donations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `donations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `leaderboard_id` varchar(45) DEFAULT NULL,
  `donor_id` varchar(45) DEFAULT NULL,
  `donor_ip` varchar(45) DEFAULT NULL,
  `anon_donation` int(1) NOT NULL DEFAULT '0',
  `amount` int(11) DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `donations`
--

LOCK TABLES `donations` WRITE;
/*!40000 ALTER TABLE `donations` DISABLE KEYS */;
/*!40000 ALTER TABLE `donations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game_data`
--

DROP TABLE IF EXISTS `game_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player_name` varchar(20) DEFAULT NULL,
  `lifetime_points` int(11) NOT NULL DEFAULT '0',
  `points` int(11) NOT NULL DEFAULT '0',
  `tool` int(11) NOT NULL DEFAULT '1',
  `warnings` int(11) NOT NULL DEFAULT '0',
  `last_update` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game_data`
--

LOCK TABLES `game_data` WRITE;
/*!40000 ALTER TABLE `game_data` DISABLE KEYS */;
INSERT INTO `game_data` VALUES (1,'A',0,108280,5,0,1473958648),(2,'andy',0,7,1,0,1474452915),(3,'andy2',0,7,1,0,1474453055),(4,'bleedtheway',0,7,1,0,1474533770),(6,'BleedTheWay12',0,7,1,0,1474536051),(7,'BleedTheWay1',0,7,1,0,1474536117),(8,'BleedTheWay2',0,7,1,0,1474536150);
/*!40000 ALTER TABLE `game_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leaderboard_player_all_time`
--

DROP TABLE IF EXISTS `leaderboard_player_all_time`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leaderboard_player_all_time` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player` varchar(45) DEFAULT NULL,
  `points` int(11) DEFAULT NULL,
  `last_update` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leaderboard_player_all_time`
--

LOCK TABLES `leaderboard_player_all_time` WRITE;
/*!40000 ALTER TABLE `leaderboard_player_all_time` DISABLE KEYS */;
INSERT INTO `leaderboard_player_all_time` VALUES (1,'bleedtheway',300002,'1474263309'),(2,'RhiannonQ71H',164905,'1474263238'),(3,'ArthurL73J',1430,'1474263239'),(4,'JenniferY27R',42136,'1474263239'),(5,'HerrodN55T',36394,'1474263239'),(6,'EmmanuelM09J',216345,'1474263239'),(7,'SybilY15T',82265,'1474263239'),(8,'ImaniB45R',126768,'1474263239'),(9,'EugeniaM06F',189379,'1474263239'),(10,'UtaA41S',256945,'1474263239'),(11,'ThaneT58G',135046,'1474263239'),(12,'StewartK44M',197625,'1474263239'),(13,'PiperR72S',125227,'1474263239'),(14,'MeghanV26D',5813,'1474263239'),(15,'AmosS39B',40608,'1474263239'),(16,'PatienceE79U',235436,'1474263239'),(17,'CassadyU32B',224765,'1474263239'),(18,'HamiltonT85A',129971,'1474263239'),(19,'AphroditeZ49Y',299253,'1474263239'),(20,'NadineK38H',296219,'1474263239'),(21,'QuonH52Y',240260,'1474263239'),(22,'RajaJ18S',249106,'1474263239'),(23,'AlecI48V',22899,'1474263239'),(24,'MollieX34D',221124,'1474263239'),(25,'DariusI64D',1559,'1474263239'),(26,'EllaG92F',158305,'1474263239'),(27,'AmalH23G',126894,'1474263239'),(28,'DarylV65L',180991,'1474263239'),(29,'IvoryO37S',74733,'1474263239'),(30,'ArielS65Q',215518,'1474263239'),(31,'AquilaK55A',264076,'1474263239'),(32,'CynthiaD86W',127934,'1474263239'),(33,'NoahX00Z',130370,'1474263240'),(34,'CaesarY81V',259027,'1474263240'),(35,'JaquelynG75Z',120868,'1474263240'),(36,'ChesterL87Z',186035,'1474263240'),(37,'JamaliaN66P',30621,'1474263240'),(38,'ZahirO73I',49961,'1474263240'),(39,'HedyN42V',86619,'1474263240'),(40,'SachaT59R',97393,'1474263240'),(41,'JackP79F',9305,'1474263240'),(42,'SimonJ70I',159317,'1474263240'),(43,'AbdulB96Y',126556,'1474263240'),(44,'ConanJ99S',59495,'1474263240'),(45,'EricaF78Z',87736,'1474263240'),(46,'DaphneH38A',254045,'1474263240'),(47,'AshelyF41V',45107,'1474263240'),(48,'SteelL97E',108630,'1474263240'),(49,'JoyD42Y',211955,'1474263240'),(50,'TanekY13A',24244,'1474263240'),(51,'MalikX19E',159531,'1474263240'),(52,'WynterB45A',178862,'1474263240'),(53,'ShaineU11P',204661,'1474263240'),(54,'DesireeX88X',237907,'1474263240'),(55,'ChloeI73J',151661,'1474263240'),(56,'GwendolynV93J',220957,'1474263240'),(57,'StephenA66I',220493,'1474263240'),(58,'AsherV97P',170509,'1474263240'),(59,'RonanQ21S',224886,'1474263240'),(60,'PamelaZ14G',169076,'1474263240'),(61,'SigneT83E',208041,'1474263240'),(62,'NaidaF95U',274383,'1474263240'),(63,'JackX05A',14434,'1474263240'),(64,'ShafiraI39A',64521,'1474263240'),(65,'MarshallF75L',265508,'1474263240'),(66,'VenusX82K',194955,'1474263240'),(67,'JoyQ82Q',129516,'1474263241'),(68,'JonasG72N',111927,'1474263241'),(69,'IanV69J',38169,'1474263241'),(70,'TeaganH75Z',91084,'1474263241'),(71,'SierraV22B',79394,'1474263241'),(72,'AndrewI96K',51365,'1474263241'),(73,'HeatherU14X',190186,'1474263241'),(74,'AmenaC04U',138189,'1474263241'),(75,'KimberleyP93K',148859,'1474263241'),(76,'HoytS18Y',31274,'1474263241'),(77,'KyleeV68H',93850,'1474263241'),(78,'LeighJ85Q',90907,'1474263241'),(79,'JinL11N',12167,'1474263241'),(80,'OttoI51B',202769,'1474263241'),(81,'MaiaF05O',213438,'1474263241'),(82,'WendyL00S',103929,'1474263241'),(83,'IvorG08Z',65611,'1474263241'),(84,'PatrickZ57I',233453,'1474263241'),(85,'AmeliaU20G',45930,'1474263241'),(86,'HamishB75C',264489,'1474263241'),(87,'LilaM07N',133569,'1474263241'),(88,'AbbotC80S',66354,'1474263241'),(89,'FerdinandS21B',58991,'1474263241'),(90,'IndiraO25J',26090,'1474263241'),(91,'FlynnD46Q',283397,'1474263241'),(92,'SonyaV10N',133814,'1474263241'),(93,'RudyardZ26C',45901,'1474263241'),(94,'DieterX24U',29962,'1474263241'),(95,'DahliaM95W',234037,'1474263241'),(96,'CalistaW77C',258658,'1474263242'),(97,'OwenL13O',174662,'1474263242'),(98,'SheilaP04B',134615,'1474263242'),(99,'RoannaE54O',204089,'1474263242'),(100,'HaleyX67S',137508,'1474263242'),(101,'BrianQ24T',27252,'1474263242');
/*!40000 ALTER TABLE `leaderboard_player_all_time` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leaderboard_player_daily`
--

DROP TABLE IF EXISTS `leaderboard_player_daily`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leaderboard_player_daily` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player` varchar(45) DEFAULT NULL,
  `points` int(11) DEFAULT NULL,
  `last_update` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leaderboard_player_daily`
--

LOCK TABLES `leaderboard_player_daily` WRITE;
/*!40000 ALTER TABLE `leaderboard_player_daily` DISABLE KEYS */;
INSERT INTO `leaderboard_player_daily` VALUES (1,'BleedTheWay',133172,'1474845162');
/*!40000 ALTER TABLE `leaderboard_player_daily` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leaderboard_player_monthly`
--

DROP TABLE IF EXISTS `leaderboard_player_monthly`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leaderboard_player_monthly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player` varchar(45) DEFAULT NULL,
  `points` int(11) DEFAULT NULL,
  `last_update` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leaderboard_player_monthly`
--

LOCK TABLES `leaderboard_player_monthly` WRITE;
/*!40000 ALTER TABLE `leaderboard_player_monthly` DISABLE KEYS */;
INSERT INTO `leaderboard_player_monthly` VALUES (1,'bleedtheway',300002,'1474263309'),(2,'RhiannonQ71H',164905,'1474263238'),(3,'ArthurL73J',1430,'1474263238'),(4,'JenniferY27R',42136,'1474263239'),(5,'HerrodN55T',36394,'1474263239'),(6,'EmmanuelM09J',216345,'1474263239'),(7,'SybilY15T',82265,'1474263239'),(8,'ImaniB45R',126768,'1474263239'),(9,'EugeniaM06F',189379,'1474263239'),(10,'UtaA41S',256945,'1474263239'),(11,'ThaneT58G',135046,'1474263239'),(12,'StewartK44M',197625,'1474263239'),(13,'PiperR72S',125227,'1474263239'),(14,'MeghanV26D',5813,'1474263239'),(15,'AmosS39B',40608,'1474263239'),(16,'PatienceE79U',235436,'1474263239'),(17,'CassadyU32B',224765,'1474263239'),(18,'HamiltonT85A',129971,'1474263239'),(19,'AphroditeZ49Y',299253,'1474263239'),(20,'NadineK38H',296219,'1474263239'),(21,'QuonH52Y',240260,'1474263239'),(22,'RajaJ18S',249106,'1474263239'),(23,'AlecI48V',22899,'1474263239'),(24,'MollieX34D',221124,'1474263239'),(25,'DariusI64D',1559,'1474263239'),(26,'EllaG92F',158305,'1474263239'),(27,'AmalH23G',126894,'1474263239'),(28,'DarylV65L',180991,'1474263239'),(29,'IvoryO37S',74733,'1474263239'),(30,'ArielS65Q',215518,'1474263239'),(31,'AquilaK55A',264076,'1474263239'),(32,'CynthiaD86W',127934,'1474263239'),(33,'NoahX00Z',130370,'1474263239'),(34,'CaesarY81V',259027,'1474263240'),(35,'JaquelynG75Z',120868,'1474263240'),(36,'ChesterL87Z',186035,'1474263240'),(37,'JamaliaN66P',30621,'1474263240'),(38,'ZahirO73I',49961,'1474263240'),(39,'HedyN42V',86619,'1474263240'),(40,'SachaT59R',97393,'1474263240'),(41,'JackP79F',9305,'1474263240'),(42,'SimonJ70I',159317,'1474263240'),(43,'AbdulB96Y',126556,'1474263240'),(44,'ConanJ99S',59495,'1474263240'),(45,'EricaF78Z',87736,'1474263240'),(46,'DaphneH38A',254045,'1474263240'),(47,'AshelyF41V',45107,'1474263240'),(48,'SteelL97E',108630,'1474263240'),(49,'JoyD42Y',211955,'1474263240'),(50,'TanekY13A',24244,'1474263240'),(51,'MalikX19E',159531,'1474263240'),(52,'WynterB45A',178862,'1474263240'),(53,'ShaineU11P',204661,'1474263240'),(54,'DesireeX88X',237907,'1474263240'),(55,'ChloeI73J',151661,'1474263240'),(56,'GwendolynV93J',220957,'1474263240'),(57,'StephenA66I',220493,'1474263240'),(58,'AsherV97P',170509,'1474263240'),(59,'RonanQ21S',224886,'1474263240'),(60,'PamelaZ14G',169076,'1474263240'),(61,'SigneT83E',208041,'1474263240'),(62,'NaidaF95U',274383,'1474263240'),(63,'JackX05A',14434,'1474263240'),(64,'ShafiraI39A',64521,'1474263240'),(65,'MarshallF75L',265508,'1474263240'),(66,'VenusX82K',194955,'1474263240'),(67,'JoyQ82Q',129516,'1474263241'),(68,'JonasG72N',111927,'1474263241'),(69,'IanV69J',38169,'1474263241'),(70,'TeaganH75Z',91084,'1474263241'),(71,'SierraV22B',79394,'1474263241'),(72,'AndrewI96K',51365,'1474263241'),(73,'HeatherU14X',190186,'1474263241'),(74,'AmenaC04U',138189,'1474263241'),(75,'KimberleyP93K',148859,'1474263241'),(76,'HoytS18Y',31274,'1474263241'),(77,'KyleeV68H',93850,'1474263241'),(78,'LeighJ85Q',90907,'1474263241'),(79,'JinL11N',12167,'1474263241'),(80,'OttoI51B',202769,'1474263241'),(81,'MaiaF05O',213438,'1474263241'),(82,'WendyL00S',103929,'1474263241'),(83,'IvorG08Z',65611,'1474263241'),(84,'PatrickZ57I',233453,'1474263241'),(85,'AmeliaU20G',45930,'1474263241'),(86,'HamishB75C',264489,'1474263241'),(87,'LilaM07N',133569,'1474263241'),(88,'AbbotC80S',66354,'1474263241'),(89,'FerdinandS21B',58991,'1474263241'),(90,'IndiraO25J',26090,'1474263241'),(91,'FlynnD46Q',283397,'1474263241'),(92,'SonyaV10N',133814,'1474263241'),(93,'RudyardZ26C',45901,'1474263241'),(94,'DieterX24U',29962,'1474263241'),(95,'DahliaM95W',234037,'1474263241'),(96,'CalistaW77C',258658,'1474263242'),(97,'OwenL13O',174662,'1474263242'),(98,'SheilaP04B',134615,'1474263242'),(99,'RoannaE54O',204089,'1474263242'),(100,'HaleyX67S',137508,'1474263242'),(101,'BrianQ24T',27252,'1474263242');
/*!40000 ALTER TABLE `leaderboard_player_monthly` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leaderboard_player_weekly`
--

DROP TABLE IF EXISTS `leaderboard_player_weekly`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leaderboard_player_weekly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player` varchar(45) DEFAULT NULL,
  `points` int(11) DEFAULT NULL,
  `last_update` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leaderboard_player_weekly`
--

LOCK TABLES `leaderboard_player_weekly` WRITE;
/*!40000 ALTER TABLE `leaderboard_player_weekly` DISABLE KEYS */;
INSERT INTO `leaderboard_player_weekly` VALUES (1,'bleedtheway',300002,'1474263309'),(2,'RhiannonQ71H',164905,'1474263238'),(3,'ArthurL73J',1430,'1474263238'),(4,'JenniferY27R',42136,'1474263239'),(5,'HerrodN55T',36394,'1474263239'),(6,'EmmanuelM09J',216345,'1474263239'),(7,'SybilY15T',82265,'1474263239'),(8,'ImaniB45R',126768,'1474263239'),(9,'EugeniaM06F',189379,'1474263239'),(10,'UtaA41S',256945,'1474263239'),(11,'ThaneT58G',135046,'1474263239'),(12,'StewartK44M',197625,'1474263239'),(13,'PiperR72S',125227,'1474263239'),(14,'MeghanV26D',5813,'1474263239'),(15,'AmosS39B',40608,'1474263239'),(16,'PatienceE79U',235436,'1474263239'),(17,'CassadyU32B',224765,'1474263239'),(18,'HamiltonT85A',129971,'1474263239'),(19,'AphroditeZ49Y',299253,'1474263239'),(20,'NadineK38H',296219,'1474263239'),(21,'QuonH52Y',240260,'1474263239'),(22,'RajaJ18S',249106,'1474263239'),(23,'AlecI48V',22899,'1474263239'),(24,'MollieX34D',221124,'1474263239'),(25,'DariusI64D',1559,'1474263239'),(26,'EllaG92F',158305,'1474263239'),(27,'AmalH23G',126894,'1474263239'),(28,'DarylV65L',180991,'1474263239'),(29,'IvoryO37S',74733,'1474263239'),(30,'ArielS65Q',215518,'1474263239'),(31,'AquilaK55A',264076,'1474263239'),(32,'CynthiaD86W',127934,'1474263239'),(33,'NoahX00Z',130370,'1474263239'),(34,'CaesarY81V',259027,'1474263240'),(35,'JaquelynG75Z',120868,'1474263240'),(36,'ChesterL87Z',186035,'1474263240'),(37,'JamaliaN66P',30621,'1474263240'),(38,'ZahirO73I',49961,'1474263240'),(39,'HedyN42V',86619,'1474263240'),(40,'SachaT59R',97393,'1474263240'),(41,'JackP79F',9305,'1474263240'),(42,'SimonJ70I',159317,'1474263240'),(43,'AbdulB96Y',126556,'1474263240'),(44,'ConanJ99S',59495,'1474263240'),(45,'EricaF78Z',87736,'1474263240'),(46,'DaphneH38A',254045,'1474263240'),(47,'AshelyF41V',45107,'1474263240'),(48,'SteelL97E',108630,'1474263240'),(49,'JoyD42Y',211955,'1474263240'),(50,'TanekY13A',24244,'1474263240'),(51,'MalikX19E',159531,'1474263240'),(52,'WynterB45A',178862,'1474263240'),(53,'ShaineU11P',204661,'1474263240'),(54,'DesireeX88X',237907,'1474263240'),(55,'ChloeI73J',151661,'1474263240'),(56,'GwendolynV93J',220957,'1474263240'),(57,'StephenA66I',220493,'1474263240'),(58,'AsherV97P',170509,'1474263240'),(59,'RonanQ21S',224886,'1474263240'),(60,'PamelaZ14G',169076,'1474263240'),(61,'SigneT83E',208041,'1474263240'),(62,'NaidaF95U',274383,'1474263240'),(63,'JackX05A',14434,'1474263240'),(64,'ShafiraI39A',64521,'1474263240'),(65,'MarshallF75L',265508,'1474263240'),(66,'VenusX82K',194955,'1474263240'),(67,'JoyQ82Q',129516,'1474263241'),(68,'JonasG72N',111927,'1474263241'),(69,'IanV69J',38169,'1474263241'),(70,'TeaganH75Z',91084,'1474263241'),(71,'SierraV22B',79394,'1474263241'),(72,'AndrewI96K',51365,'1474263241'),(73,'HeatherU14X',190186,'1474263241'),(74,'AmenaC04U',138189,'1474263241'),(75,'KimberleyP93K',148859,'1474263241'),(76,'HoytS18Y',31274,'1474263241'),(77,'KyleeV68H',93850,'1474263241'),(78,'LeighJ85Q',90907,'1474263241'),(79,'JinL11N',12167,'1474263241'),(80,'OttoI51B',202769,'1474263241'),(81,'MaiaF05O',213438,'1474263241'),(82,'WendyL00S',103929,'1474263241'),(83,'IvorG08Z',65611,'1474263241'),(84,'PatrickZ57I',233453,'1474263241'),(85,'AmeliaU20G',45930,'1474263241'),(86,'HamishB75C',264489,'1474263241'),(87,'LilaM07N',133569,'1474263241'),(88,'AbbotC80S',66354,'1474263241'),(89,'FerdinandS21B',58991,'1474263241'),(90,'IndiraO25J',26090,'1474263241'),(91,'FlynnD46Q',283397,'1474263241'),(92,'SonyaV10N',133814,'1474263241'),(93,'RudyardZ26C',45901,'1474263241'),(94,'DieterX24U',29962,'1474263241'),(95,'DahliaM95W',234037,'1474263241'),(96,'CalistaW77C',258658,'1474263241'),(97,'OwenL13O',174662,'1474263242'),(98,'SheilaP04B',134615,'1474263242'),(99,'RoannaE54O',204089,'1474263242'),(100,'HaleyX67S',137508,'1474263242'),(101,'BrianQ24T',27252,'1474263242');
/*!40000 ALTER TABLE `leaderboard_player_weekly` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leaderboard_stream_all_time`
--

DROP TABLE IF EXISTS `leaderboard_stream_all_time`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leaderboard_stream_all_time` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stream` varchar(45) DEFAULT NULL,
  `points` int(11) DEFAULT NULL,
  `last_update` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leaderboard_stream_all_time`
--

LOCK TABLES `leaderboard_stream_all_time` WRITE;
/*!40000 ALTER TABLE `leaderboard_stream_all_time` DISABLE KEYS */;
/*!40000 ALTER TABLE `leaderboard_stream_all_time` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leaderboard_stream_daily`
--

DROP TABLE IF EXISTS `leaderboard_stream_daily`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leaderboard_stream_daily` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stream` varchar(45) DEFAULT NULL,
  `points` int(11) DEFAULT NULL,
  `last_update` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leaderboard_stream_daily`
--

LOCK TABLES `leaderboard_stream_daily` WRITE;
/*!40000 ALTER TABLE `leaderboard_stream_daily` DISABLE KEYS */;
INSERT INTO `leaderboard_stream_daily` VALUES (1,'BleedTheWay',2480,'1474851408'),(2,'saltybet',3990,'1474796091'),(3,'reynad27',9940,'1474796261');
/*!40000 ALTER TABLE `leaderboard_stream_daily` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leaderboard_stream_monthly`
--

DROP TABLE IF EXISTS `leaderboard_stream_monthly`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leaderboard_stream_monthly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stream` varchar(45) DEFAULT NULL,
  `points` int(11) DEFAULT NULL,
  `last_update` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leaderboard_stream_monthly`
--

LOCK TABLES `leaderboard_stream_monthly` WRITE;
/*!40000 ALTER TABLE `leaderboard_stream_monthly` DISABLE KEYS */;
/*!40000 ALTER TABLE `leaderboard_stream_monthly` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leaderboard_stream_weekly`
--

DROP TABLE IF EXISTS `leaderboard_stream_weekly`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leaderboard_stream_weekly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stream` varchar(45) DEFAULT NULL,
  `points` int(11) DEFAULT NULL,
  `last_update` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leaderboard_stream_weekly`
--

LOCK TABLES `leaderboard_stream_weekly` WRITE;
/*!40000 ALTER TABLE `leaderboard_stream_weekly` DISABLE KEYS */;
/*!40000 ALTER TABLE `leaderboard_stream_weekly` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `requests`
--

DROP TABLE IF EXISTS `requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `rkey` varchar(20) NOT NULL,
  `expire` datetime NOT NULL,
  `type` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `requests`
--

LOCK TABLES `requests` WRITE;
/*!40000 ALTER TABLE `requests` DISABLE KEYS */;
INSERT INTO `requests` VALUES (37,7,'72q4e9szMLCA8919u5HO','2016-09-22 04:52:07','activation');
/*!40000 ALTER TABLE `requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `hash` varchar(40) NOT NULL,
  `expiredate` datetime NOT NULL,
  `ip` varchar(39) NOT NULL,
  `agent` varchar(200) NOT NULL,
  `cookie_crc` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
INSERT INTO `sessions` VALUES (16,11,'1ebdf4ddbd138231c64793d0a8ae63900db7032e','2016-12-12 14:54:31','::1','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:48.0) Gecko/20100101 Firefox/48.0','ed25271b9c5c9c814a6bda973eb61ce758efbc64'),(41,15,'1b0a2b48ed9c2a774eb389e8a5a3a6028aad04d8','2016-09-15 03:03:48','::1','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:48.0) Gecko/20100101 Firefox/48.0','56524f692e190bd08a99a876b2552dc8ea344a53'),(43,17,'2836aeff0e91e90b25dec023ed6004b46be9bb07','2016-09-15 09:22:09','::1','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:48.0) Gecko/20100101 Firefox/48.0','0b2642c347dc7b1dfc9bb689437fdee5b7a916b3');
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `streams`
--

DROP TABLE IF EXISTS `streams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `streams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `streamer` varchar(45) DEFAULT NULL,
  `source` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `streams`
--

LOCK TABLES `streams` WRITE;
/*!40000 ALTER TABLE `streams` DISABLE KEYS */;
INSERT INTO `streams` VALUES (1,'BleedTheWay','Twitch'),(2,'saltybet','twitch'),(3,'reynad27','twitch');
/*!40000 ALTER TABLE `streams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tool_data`
--

DROP TABLE IF EXISTS `tool_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tool_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `multiplier` int(11) NOT NULL DEFAULT '1',
  `description` varchar(500) DEFAULT NULL,
  `material` varchar(45) DEFAULT NULL,
  `cost` int(11) DEFAULT NULL,
  `ppc` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tool_data`
--

LOCK TABLES `tool_data` WRITE;
/*!40000 ALTER TABLE `tool_data` DISABLE KEYS */;
INSERT INTO `tool_data` VALUES (1,'Your Fist',1,'Where it all humbly begins. You sitting here, beating on a rock with your fists. Not the smartest move, yet here you are. determined to get enough points to buy something better. ','Flesh',0,1),(2,'Wooden Pickaxe',1,'A wooden pickaxe... Who thought the wood pickaxe was a good idea... OH well! Here it is.. the staple of mining games... the wooden pickaxe. This thing doesn\'t change a whole lot, you still get 1 point per click, but hey, you\'ve now got a cool looking pickaxe! You\'re welcome?','Wood',100,1),(3,'Copper Pickaxe',11,'A Copper Pickaxe. Just like the wooden version, with less splintering!','Copper',1318,11),(4,'Iron Pickaxe',47,'A strong iron pickaxe. Made from iron... what more do you want on this one! ','Iron',5955,47),(5,'Diamond Pickaxe',128,'This 22,200 carat pickaxe is sure to make her happy! Break the bank with this wonderful Steve cut Diamond Pickaxe from Jared today! ','Diamond',17365,128),(6,'Hellfire Pickaxe',280,'This fiery pickaxe looks like it came from another world. Some say it was found in the deepest mines of Golgarra, encased in a floating orb of lava, surrounded by demonic runes. Others say it came from some guy named Tom who makes really good cosplay gear.','Demon Forged Mythril',39826,280);
/*!40000 ALTER TABLE `tool_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(60) DEFAULT NULL,
  `isactive` tinyint(1) NOT NULL DEFAULT '0',
  `dt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'bleedtheway@gmail.com','$2y$10$GXhPyIudLZLQMyw2LxtQU.k58xo9Ru.VLUZlquOlhNrfWPupq3cvG',1,'2016-09-15 16:57:28'),(2,'andyrob7@gmail.com','$2y$10$vSt9YFsbb6vsDB697v45Xe5LrgJDQg7sUORQimWzSJy091IpYC5dO',0,'2016-09-21 10:15:15'),(3,'andyrob71@gmail.com','$2y$10$TlaZHfzWl1BA8YR8/fpdfOGuERzDgBVIjC7mPoCiQzHufnzy9c2QW',0,'2016-09-21 10:17:35'),(4,'bleedtheway360@gmail.com','$2y$10$cxKi51Cr/2H8y0Dg3JJHpuBdOMF4BJoJmTtsKz5YOypXCtw2ZDGXG',0,'2016-09-22 08:42:49'),(5,'bleedtheway@bleedtheway.com','$2y$10$epNTc4XMtSkWULL0NyCz.uAqZBedZS57TjrS2NWE8b6sBri1sPOYu',0,'2016-09-22 09:19:27'),(6,'bleedtheway2@bleedtheway.com','$2y$10$csPDvCSKrZ6dgTA2v4Gjk.3iUVHrhES4.1nGro8bJclzuQmiv1cTq',0,'2016-09-22 09:20:51'),(7,'bleedtheway3@bleedtheway.com','$2y$10$s9IM401l3OA5xuQ6Jvr9IuPNau13EuZ5kjsqMLRATHi1AuWDcvYP6',0,'2016-09-22 09:21:57'),(8,'bleedtheway4@bleedtheway.com','$2y$10$WcPYZ8Cwor0ZB8ICl7nuf.1yTTgP9xjHgYqPXLmnekhO1UI5JRegK',0,'2016-09-22 09:22:30');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-09-25 19:59:28
